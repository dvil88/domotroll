<?php
include_once('cli.php');

$logger = new logger();


$tunPidFile = '/dev/shm/tunnels.pid';
if( file_exists($tunPidFile) ){
	$logger->debug('PID found');
	// check if tunnels are running
	$pid = file_get_contents($tunPidFile);
	if( file_exists('/proc/'.$pid) ){
		$logger->warn('Process running');
		// There is a process already running
		exit;
	}
	$logger->info('Process not running');
}


file_put_contents($tunPidFile, getmypid());

$tc = new tunnelConnection();

$openTunnels = $tc->getOpenTunnels();
$allowedTunnels = $tc->getTunnelsAllowed();
$tunnelsToOpen = array_diff($allowedTunnels, $openTunnels);

foreach( $tunnelsToOpen as $tunnel ){
	$logger->debug('Open '.$tunnel);
	$opened = $tc->openTunnel($tunnel);
	if( $opened ){ $logger->info($tunnel.' opened'); }
	else{ $logger->error($tunnel.' not opened'); }
}

// Restart tunnels
$logger->debug('Restarting tunnels');
$tc->launchRestart();

unlink($tunPidFile);