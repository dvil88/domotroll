<?php

class alerts_TB extends _mongo{
	public $table = 'alerts';
	public $fields = [
		'_id'=>'TEXT',

		'userId'=>'TEXT',

		'brandId'=>'TEXT',
		'modelId'=>'TEXT',
		'yearMin'=>'TEXT',
		'yearMax'=>'TEXT',
		'sellerType'=>'TEXT',
		'transmissionType'=>'TEXT',
		'kmMin'=>'TEXT',
		'kmMax'=>'TEXT',
		'priceMin'=>'TEXT',
		'priceMax'=>'TEXT',
		'provinceId'=>'TEXT',

		'date'=>'TEXT',
	];
	public $indexes = [
	];

	public function __construct(){
		$this->server = 'mongodb://'.config::$mongoConfig['user'].':'.config::$mongoConfig['pass'].'@'.config::$mongoConfig['host'].':'.config::$mongoConfig['port'];
		$this->db = config::$mongoConfig['database'];
	}

	function validate(&$data = [],&$oldData = []){
		if( isset($data['brandId']) && !is_array($data['brandId']) ){ $data['brandId'] = [$data['brandId']]; }
		if( isset($data['modelId']) && !is_array($data['modelId']) ){ $data['modelId'] = [$data['modelId']]; }

		if( isset($data['yearMin']) ){ $data['yearMin'] = (int)$data['yearMin']; }
		if( isset($data['yearMax']) ){ $data['yearMax'] = (int)$data['yearMax']; }
		if( isset($data['kmMin']) ){ $data['kmMin'] = (int)$data['kmMin']; }
		if( isset($data['kmMax']) ){ $data['kmMax'] = (int)$data['kmMax']; }
		if( isset($data['priceMin']) ){ $data['priceMin'] = (int)$data['priceMin']; }
		if( isset($data['priceMax']) ){ $data['priceMax'] = (int)$data['priceMax']; }

		if( !isset($data['date']) ){ $data['date'] = time(); }
		return $data;
	}
}