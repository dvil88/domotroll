<?php
$GLOBALS['inc']['common'] = array(
	'replace'=>0,
	'css.files'=>array(),
	'js.files'=>array(),
	'output'=>'',
	'photo.path'=>'../images/default/'
	,'page.404'=>'404'
);
$GLOBALS['COMMON']['SNIPPETCACHE'] = [];
if( ini_get('pcre.backtrack_limit') < 2000000){ini_set('pcre.backtrack_limit','2M');}

class common{
	protected $template;

	private $path;
	private $ext;
	private $base;
	private $output;

	public function __construct(){
		$this->path = '../views/';
		$this->ext = '.php';
		$this->base = 'base';

		// $this->template['w.localhost'] = $GLOBALS['w.localhost'];
		$this->template['w.indexURL'] = $GLOBALS['w.indexURL'];
		$this->template['w.currentURL'] = $GLOBALS['w.currentURL'];
		// $this->template['w.page'] = $GLOBALS['w.page'];
		// $this->template['w.isMobile'] = $GLOBALS['w.isMobile'];
	}

	public function setPath($path = ''){ $this->path = $path; }
	public function setBase($base = ''){ $this->base = $base; }
	public function setExt($ext = ''){ $this->ext = $ext; }

	private function loadScript($script = '',$attribs = ['async']){
		if ( !$script ) { return false; }
		$index = md5($script);
		if ( !isset($GLOBALS['inc']['common']['js.files'][$index]) ) { $GLOBALS['inc']['common']['js.files'][$index] = ['url'=>$script,'attr'=>$attribs]; }
	}
	private function loadStyle($style = ''){$GLOBALS['inc']['common']['css.files'][] = $style;}
	private function findKword2($kword,$pool = false){
		/* For debug */
		if( $pool === false ){$pool = &$GLOBALS;}
		$function = __FUNCTION__;

		if( isset($pool[$kword]) ){return $pool[$kword];}
		$parts = explode('_',$kword);

		$rest  = [];
		while( ($pop = array_pop($parts)) !== null ){
			$rest[] = $pop;
			$try = implode('_',$parts);
			if( isset($pool[$try]) ){
				if( !$rest ){return $pool[$try];}
				$rest = array_reverse($rest);
				$rest = implode('_',$rest);
				return $function($rest,$pool[$try]);
			}
		}
		return false;
	}
	private function findKword($kword,$pool = false){
		if( $pool === false ){$pool = &$GLOBALS;}
		$function = __FUNCTION__;

		if( isset($pool[$kword]) ){return $pool[$kword];}
		$parts = explode('_',$kword);
		$rest  = [];
		while( ($pop = array_pop($parts)) !== null ){
			$rest[] = $pop;
			$try = implode('_',$parts);
			if( isset($pool[$try]) ){
				if( !$rest ){return $pool[$try];}
				$rest = array_reverse($rest);
				$rest = implode('_',$rest);
				return $this->$function($rest,$pool[$try]);
			}
		}
		return false;
	}
	private function resetReplaceIteration(){ $GLOBALS['inc']['common']['replace'] = 0; }
	private function replaceIncludes($blob){
		$regexp = '!{{@(?<template>[a-zA-Z0-9_\.\-/]+)}}!sm';

		$GLOBALS['inc']['common']['replace'] = 0;
		while( preg_match($regexp,$blob,$m) ){
			$GLOBALS['inc']['common']['replace']++;
			if($GLOBALS['inc']['common']['replace'] > 20){echo 'max replaces';exit;}

			$blob = preg_replace_callback($regexp,function($m){
				return $this->loadSnippet($m['template']);
			},$blob);
		}

		return $blob;
	}
	private function replaceLogic($blob,$pool = false,$reps = false){
		/* Ahora reemplazamos parte de la lógica */
		$GLOBALS['inc']['common']['replace'] = 0;
		while( preg_match('/{{[#\^]([a-zA-Z0-9_\.\-]+)}}(.*?){{\/\1}}/sm',$blob) ){
			$GLOBALS['inc']['common']['replace']++;
			if($GLOBALS['inc']['common']['replace'] > 20){echo 'max replaces';exit;}

			$blob = preg_replace_callback('!{{(?<operator>[#\^])(?<word>[a-zA-Z0-9_\.\-]+)}}(?<snippet>.*?){{/\2}}!sm',function($m) use (&$pool){
				//if( $m['word'] == 'install_steps_config' ){var_dump($this->findKword2($m['word'],$pool));exit;}
				if( ($word = $this->findKword($m['word'],$pool)) === false ){
					$word = $this->findKword($m['word'], $this->template);
				}
				if( !$word ){
					if( $m['operator'] == '^' ){return $m['snippet'];}
					return '';
				}
				if( $m['operator'] == '^' ){return '';}

				/* INI-Soporte para Arrays */
				if( is_array($word) ){
					$blob    = '';
					$snippet = $m['snippet'];
					foreach( $word as $elem ){
						if( !is_array($elem) ){$elem = ['.'=>$elem];}
						$blob .= $this->replaceInTemplate($snippet,$elem);
					}
					return $blob;
				}
				/* END-Soporte para Arrays */
				return $m['snippet'];
			},$blob);
		}

		return $blob;
	}
	private function replaceInTemplate($blob,$pool = false,$reps = false){
		$blob = $this->replaceIncludes($blob);
		$blob = $this->replaceLogic($blob,$pool,$reps);

		$GLOBALS['inc']['common']['replace'] = 0;
		$notFound = [];
		while( ($hasElems = preg_match_all('/{{[a-zA-Z0-9_\.\-]+}}/',$blob,$reps)) ){
			$GLOBALS['inc']['common']['replace']++;
			$reps = array_unique($reps[0]);
			foreach($reps as $k=>$rep){
				$kword = substr($rep,2,-2);
				if( ($word = $this->findKword($kword,$pool)) === false ){
					unset($reps[$k]);
					$notFound[$kword] = '';
					continue;
				}
				//if( is_array($word) ){unset($reps[$k]);$word = print_r($word,1);}
				if( is_array($word) ){unset($reps[$k]);$word = '';continue;}
				$blob = str_replace($rep,$word,$blob);
				$blob = $this->replaceIncludes($blob);
				$blob = $this->replaceLogic($blob,$pool,$reps);
				continue;
			}
			if( !$reps ){break;}
			if($GLOBALS['inc']['common']['replace'] > 20){print_r($notFound);print_r($reps);exit;}
		}

		return $blob;
	}
	public function renderTemplate($t = false, $b = false){
		// $this->template = &$GLOBALS['TEMPLATE'];
		$pathTemp = $this->path.$t.$this->ext;
		$pathBase = $this->path.($b ? $b : $this->base).$this->ext;

		if( $this->ext == '.php' ){
			ob_start();include($pathTemp);$this->template['MAIN'] = ob_get_contents();ob_end_clean();
			ob_start();include($pathBase);$this->output = ob_get_contents();ob_end_clean();
		} else {
			if($t){ $this->template['MAIN'] = file_get_contents($pathTemp); }
			$this->output = file_get_contents($pathBase);
		}

		$GLOBALS['debug'] = false;
		/* INI-BLOG_SCRIPT_VARS */
		if( count($GLOBALS['inc']['common']['js.files']) ){
			$this->template['PAGE.SCRIPT'] = array_map(function($n){
				if( substr($n['url'],0,4) != 'http' ){$n['url'] = '{{w.indexURL}}'.$n['url'];}
				return '<script type="text/javascript" src="'.$n['url'].'" '.implode(' ',$n['attr']).'></script>';
			},$GLOBALS['inc']['common']['js.files']);
			$this->template['PAGE.SCRIPT'] = implode(PHP_EOL,$this->template['PAGE.SCRIPT']);
		}
		if( count($GLOBALS['inc']['common']['css.files']) ){
			$this->template['PAGE.STYLE'] = array_map(function($n){
				if( substr($n,0,4) != 'http' ){$n = '{{w.indexURL}}'.$n;}
				return '<link href="'.$n.'" rel="stylesheet">';
			},$GLOBALS['inc']['common']['css.files']);$this->template['PAGE.STYLE'] = implode(PHP_EOL,$this->template['PAGE.STYLE']);}
		/* END-BLOG_SCRIPT_VARS */
		/* INI-META */
		if(isset($this->template['META.DESCRIPTION'])){$this->template['META.DESCRIPTION'] = str_replace('"','\'',$this->template['META.DESCRIPTION']);}
		if(isset($this->template['META.OG.IMAGE'])){$this->template['META.OG.IMAGE'] = '<meta property="og:image" content="'.$this->template['META.OG.IMAGE'].'"/>'.PHP_EOL;}
		/* END-META */
		$this->output = $this->replaceInTemplate($this->output, $this->template);
		$this->output = preg_replace('/{{[a-zA-Z0-9_\.\-]+}}/','', $this->output);

		return $this->output;
	}
	public function loadSnippet($s = false,$pool = false,$sname = false){
		$file = $this->path.$s.$this->ext;
		if(!$sname){$sname = $s;}
		if(!isset($GLOBALS['COMMON']['SNIPPETCACHE'][$s])){
			if(!file_exists($file)){return false;}
			ob_start();$_PARAMS = $pool;include($file);$blob = ob_get_contents();ob_end_clean();
			$GLOBALS['COMMON']['SNIPPETCACHE'][$s] = $blob;
		}
		if(!isset($blob)){$blob = $GLOBALS['COMMON']['SNIPPETCACHE'][$s];}
		if($pool){$blob = $this->replaceInTemplate($blob,$pool);}
		$GLOBALS['TEMPLATE']['SNIPPETS'][$sname] = $blob;
		return $GLOBALS['TEMPLATE']['SNIPPETS'][$sname];
	}
	public function r($hash = '',$code = false){
		if ( !$hash && $code == 404 ) { header('HTTP/1.0 404 Not Found');return $this->error404(); }
		if ( !$code ) { $code = 302; }
		if ( substr($hash,0,4) == 'http' ) { header('Location: '.$hash,true,$code); exit; }
		/* INI-Procesar parámetros GET pasados en $hash;
		 * sobreescribiraán parámetros del mismo nombre que se hayan recibido por GET en la URL */
		if ( 0 === strpos($hash, '?') ) {
			$hash = str_replace('?', '', $hash);
			foreach ( explode('&', $hash) as $get_param ){
				list($index, $value) = explode('=', $get_param, 2);
				$_GET[$index] = $value;
			}
		}
		/* END-Procesar parámetros GET pasados en $hash */
		$get_params = [];
		foreach ( $_GET as $index => $value ) { $get_params[] = $index.'='.$value; }
		$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
		header('Location: http://'.$_SERVER['SERVER_NAME'].$uri_parts[0].( $get_params ? '?'.implode('&', $get_params) : '' ), true, $code); exit;
	}
	/* INI-Pager */
	public function pagerByElements($currentPage = 1,$totalElements = 0,$perPage = 10,$buttons = 5){
		$totalPages = ceil($totalElements/$perPage);
		return $this->pagerByPages($currentPage,$totalPages,$buttons);
	}
	public function pagerByPages($currentPage = 1,$totalPages = 5,$buttons = 5){
		$currentPage = $lowerLimit = $upperLimit = min($currentPage,$totalPages);

		for($b = 1; $b < $buttons && $b < $totalPages;){
			if($lowerLimit > 1){$lowerLimit--;$b++;}
			if($b < $buttons && $upperLimit < $totalPages){$upperLimit++;$b++;}
		}

		$items = array();
		$items[0] = ($currentPage > 1) ? 'pe' : 'pd';

		for($i = $lowerLimit; $i <= $upperLimit; $i++) {
			if($i == $currentPage){$items[$i] = 'c';continue;}
			$items[$i] = $i;
		}

		$items[$i] = ($currentPage < $totalPages) ? 'ne' : 'nd';
		return $items;
	}
	public function pagerArrayToHTML($arr,$pool = false){
		$current = array_search('c',$arr);
		if( isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] ){$pool['params'] = '?'.$_SERVER['QUERY_STRING'];}

		$p = '<ul class="pager">';
		foreach($arr as $k=>$v){
			switch($v){
				case 'pd':$p .= '<li>{{prev}}</li>';break;
				case 'pe':$p .= '<li><a href="{{url}}'.(($current-1 > 1) ? '{{add}}'.($current-1) : '').'{{params}}">{{prev}}</a></li>';break;
				case 'nd':$p .= '<li>{{next}}</li>';break;
				case 'ne':$p .= '<li><a href="{{url}}{{add}}'.($current+1).'{{params}}">{{next}}</a></li>';break;
				case 'c':$p .= '<li class="current">'.$k.'</li>';break;
				default:$p .= '<li><a href="{{url}}'.(($k > 1) ? '{{add}}'.$k : '').'{{params}}">'.$k.'</a></li>';
			}
		}
		$p .= '</ul>';
		return ($pool) ? $this->replaceInTemplate($p,$pool) : $p;
	}
	/* END-Pager */

	public function noPhoto($size = false){
		$p = $GLOBALS['inc']['common']['photo.path'];
		$pname = 'no.photo.png';
		if($size && ($pname = 'no.photo.'.$size.'.png') && !file_exists($p.$pname)){$pname = 'no.photo.png';}
		$p .= $pname;

		$r = stat($p);
		$m = date('D, d M Y H:m:s \G\M\T',$r['mtime']);
		header('Last-Modified: '.$m);
		header('Cache-Control: max-age=31557600');
		header_remove('Pragma');
		if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){$d = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);$k = strtotime($m);if($d == $k){header('HTTP/1.1 304 Not Modified');exit;}}

		header('Content-type: image/png');
		readfile($p);
		exit;
	}
	public function error404(){
		$this->setBase('base.empty');
		echo $this->renderTemplate($GLOBALS['inc']['common']['page.404']);exit;
	}
}