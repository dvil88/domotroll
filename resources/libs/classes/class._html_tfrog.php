<?php
	trait __html_tfrog{
		public $entry = 'http://hmv01.colorvamp.com/hma/';
		public $token = '99ede1ae2784cd312844bfb5001467e2';
		public $iface = 'tun152';
		public $html_is_browser = true;
		use __html_fileg {
			__html_fileg::query as _html_fileg_query;
		}
		function query($url = '',$params = []){
			$_params = [];

			if (!empty($params['header'])) {
				/* Normalize headers */
				foreach ($params['header'] as $key=>$value) {
					$lower = strtolower($key);
					if ($lower == $key) {continue;}
					$params['header'][$lower] = $value;
					unset($params['header'][$key]);
				}
			}
			$post = json_encode([
				 'url'=>$url
				,'header'=>$params['header'] ?? []
				,'post'=>$params['post'] ?? []
			]);
			$_params['post'] = $post;

			$_params['header']['authorization'] = 'Bearer '.$this->token;
			$_params['header']['iface'] = $this->iface;
			$data = $this->_html_fileg_query($this->entry.'query',$_params);
			if (isset($data['errorDescription'])) {return $data;}
			return json_decode($data['page-content'],true);
		}
		function ip(){
			$_params = [];
			$_params['header']['authorization'] = 'Bearer '.$this->token;
			$_params['header']['iface'] = $this->iface;
			$data = $this->_html_fileg_query($this->entry.'ip',$_params);
			if (isset($data['errorDescription'])) {return $data;}
			$test = json_decode($data['page-content'],true);
			if ($test === NULL) {return ['errorDescription'=>'UNKNOWN_ERROR','file'=>__FILE__,'line'=>__LINE__];}
			return $test;
		}
	}
	class _html_tfrog{
		use __html_tfrog;
	}


