<?php

class valuation{
	protected $kmRanges = [
		['from'=>0, 'to'=>2500],
		['from'=>2500, 'to'=>5000],
		['from'=>5000, 'to'=>10000],
		['from'=>10000, 'to'=>20000],
		['from'=>20000, 'to'=>30000],
		['from'=>30000, 'to'=>40000],
		['from'=>40000, 'to'=>50000],
		['from'=>50000, 'to'=>60000],
		['from'=>60000, 'to'=>70000],
		['from'=>70000, 'to'=>80000],
		['from'=>80000, 'to'=>90000],
		['from'=>90000, 'to'=>100000],
		['from'=>100000, 'to'=>125000],
		['from'=>125000, 'to'=>150000],
		['from'=>150000, 'to'=>175000],
		['from'=>175000, 'to'=>200000],
		['from'=>200000, 'to'=>999999999999],
	];
	public function __construct(){
		$this->poolMakersTB = new poolMakers_TB();
		$this->poolModelsTB = new poolModels_TB();

		$this->autocasionTB = new autocasion_TB();
		$this->autoscout24TB = new autoscout24_TB();
		$this->cochesnetTB = new cochesnet_TB();
		$this->milanunciosTB = new milanuncios_TB();
		$this->wallapopTB = new wallapop_TB();

	}

	public function generateValuation(){
		$this->fp = fopen('/tmp/valuationCars.csv', 'w+');
		fputcsv($this->fp, ['makerId','makerName','modelId','modelName','range kms','year','fuel','transmissionion','km','price']);

		// Coches.net
		$this->poolMakersTB->iterator(['webpage'=>'cochesnet'], function($makerOB){
			$makerDBId = strval($makerOB['_id']);
			$this->poolModelsTB->iterator(['makerId'=>$makerDBId], function($modelOB) use ($makerOB){
				$this->cars = [];
				$this->cochesnetTB->iterator(['makerId'=>$makerOB['makerId'], 'modelId'=>$modelOB['modelId']], function($listingOB){
					if( empty($listingOB['year']) || empty($listingOB['km']) || empty($listingOB['fuelType']) || empty($listingOB['transmissionType']) ){ return; }

					$year = $listingOB['year'];
					$km = $listingOB['km'];
					$fuelType = $listingOB['fuelType'];
					$transmissionType = $listingOB['transmissionType'];

					if( !isset($this->cars[$year]) ){
						$this->cars[$year] = [];
					}

					foreach( $this->kmRanges as $range ){
						if( $km >= $range['from'] && $km < $range['to'] ){
							$rangeStr = $range['from'].'-'.$range['to'];

							if( !isset($this->cars[$year][$rangeStr]) ){ $this->cars[$year][$rangeStr] = []; }
							if( !isset($this->cars[$year][$rangeStr][$fuelType]) ){ $this->cars[$year][$rangeStr][$fuelType] = []; }
							if( !isset($this->cars[$year][$rangeStr][$fuelType][$transmissionType]) ){ $this->cars[$year][$rangeStr][$fuelType][$transmissionType] = []; }


							$this->cars[$year][$rangeStr][$fuelType][$transmissionType][] = [
								'price'=>$listingOB['price'],
								'km'=>$listingOB['km'],
								'fuel'=>$listingOB['fuelType'],
								'transmission'=>$listingOB['transmissionType']
							];

							fputcsv($this->fp, [
								$listingOB['makerId'],
								$listingOB['makerName'],
								$listingOB['modelId'],
								$listingOB['modelName'],
								$rangeStr,
								$year,
								$fuelType,
								$transmissionType,
								$listingOB['km'],
								$listingOB['price'],
							]);
						}
					}
				});

				// print_r($this->cars);
				// exit;
			});
		});

		fclose($this->fp);
	}
}