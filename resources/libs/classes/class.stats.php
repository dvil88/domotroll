<?php

class stats {
	public function __construct(){
		$this->statsTB = new stats_TB();
	}

	public function getCPU(){
		$stat1 = file('/proc/stat');
		sleep(1);
		$stat2 = file('/proc/stat');

		$info1 = explode(' ', preg_replace('/cpu +/', '', $stat1[0]));
		$info2 = explode(' ', preg_replace('/cpu +/', '', $stat2[0]));

		$diff = array();
		$diff['user'] = $info2[0] - $info1[0];
		$diff['nice'] = $info2[1] - $info1[1];
		$diff['sys'] = $info2[2] - $info1[2];
		$diff['idle'] = $info2[3] - $info1[3];
		$total = array_sum($diff);

		$cpu = [];
		foreach( $diff as $x=>$y ){
			$cpu[$x] = round($y / $total * 100, 1);
		}

		return $cpu;
	}

	public function getMemory(){
		$free = shell_exec('free');
		$free = (string)trim($free);
		$free_arr = explode("\n", $free);

		$mem = explode(' ', $free_arr[1]);
		$mem = array_filter($mem);
		$mem = array_merge($mem);

		$swap = explode(' ', $free_arr[2]);
		$swap = array_filter($swap);
		$swap = array_merge($swap);

		$ramUsage = $mem[2]/$mem[1]*100;
		$swapUsage = $swap[2]/$swap[1]*100;

		return ['ram'=>$ramUsage, 'swap'=>$swapUsage];
	}

	public function getDisks(){
		$disks = shell_exec('df -T | tail -n +2 | grep -vE "tmpfs|squashfs|udev|cifs" | awk \'{print $1 "\t" $3 "\t" $4 "\t" $5 "\t" $7}\'');
		$disks = (string)trim($disks);
		$disks_arr = explode("\n", $disks);

		$disksInfo = [];
		foreach( $disks_arr as $disk ){
			$diskData = explode("\t", $disk);
			$diskInfo[] = [
				'filesystem'=>$diskData[0],
				'size'=>$diskData[1],
				'used'=>$diskData[2],
				'available'=>$diskData[3],
				'mountPoint'=>$diskData[4],
			];
		}

		return $diskInfo;
	}

	public function getLoad(){
		$load = [
			'cores'=>trim(shell_exec('cat /proc/cpuinfo  | grep "processor" | wc -l')),
			'loadAvg'=>sys_getloadavg()
		];
		return $load;
	}

	public function getServerStats($serverId, $minDate = false){
		$clause = ['serverId'=>$serverId];
		if( $minDate ){
			$clause['date'] = ['$gt'=>(int)$minDate];
		}

		$statsOBs = $this->statsTB->getWhere($clause, ['sort'=>'date DESC', 'limit'=>'90']);

		if( !$statsOBs ){ return []; }

		foreach( $statsOBs as $statsOB ){
			$stats[$statsOB['date']] = [
				'cpu'=>round($statsOB['cpu']['user'] + $statsOB['cpu']['sys'], 2),
				'mem'=>round($statsOB['memory']['ram'], 2),
				'disks'=>round($this->calculateDisksUsage($statsOB['disks']), 2),
				'load'=>round($this->calculateLoad($statsOB['load']), 2)
			];
		}

		return $stats;
	}

	public function getServerLastStats($serverId){
		$statsOB = $this->statsTB->getSingle(['serverId'=>$serverId], ['sort'=>'date DESC']);

		if( !$statsOB ){
			return ['cpu'=>0, 'mem'=>0, 'disks'=>0, 'load'=>0];
		}

		$stats = [
			'cpu'=>$statsOB['cpu']['user'] + $statsOB['cpu']['sys'],
			'mem'=>$statsOB['memory']['ram'],
			'disks'=>$this->calculateDisksUsage($statsOB['disks']),
			'load'=>$this->calculateLoad($statsOB['load']),
		];

		return array_map('round', $stats);
	}

	public function getLastStatsDate($serverId){
		$statsOB = $this->statsTB->getSingle(['serverId'=>$serverId], ['sort'=>'date DESC']);

		if( !$statsOB ){ return 0; }
		return $statsOB['date'];
	}

	protected function calculateDisksUsage($stats){
		$diskUsage = 0;
		$diskTotal = 0;

		foreach( $stats as $stat ){
			$diskUsage += $stat['used'];
			$diskTotal += $stat['size'];
		}

		return $diskUsage / $diskTotal * 100;
	}

	protected function calculateLoad($stats){
		return $stats['loadAvg'][0] / $stats['cores'] * 100;
	}
}