<?php

class checker{
	public function __construct(){
		$this->stats = new stats();
	}
	

	public function checkServer($serverOB, $alarm = false){
		$live = $this->checkLiveStatus($serverOB['_id']);

		if( !$live || $alarm ){
			$serverUp = $this->pingServer($serverOB['ip']);

			// si responde a ping pero !$live es que el servidor está encendido pero no está funcionando el proceso de actualización
			if( $serverUp ){
				// Connect to server
				$ssh2 = new _ssh2($serverOB['ip'], ['user'=>$serverOB['user'], 'pass'=>$serverOB['password']]);
				$connection = $ssh2->connect();
				if( isset($connection['error']) ){ 
					// Server answering ping but error on ssh
					
					return false;
				}

				// Check if crontab is installed
				$crontab = (bool)$ssh2->command('if crontab -l | grep "cli.proc.php daemon" > /dev/null; then echo 1; else echo 0; fi');

				// TODO : Check if daemon is running
				// $daemon = 


				$ssh2->disconnect();
			} else {
				return false;
			}
		}

		return true;
	}

	protected function checkLiveStatus($serverId){
		$lastDate = $this->stats->getLastStatsDate($serverId);

		$timeSinceLastUpdate = time() - $lastDate;
		if( $timeSinceLastUpdate > 600 ){
			return false;
		}
		return true;
	}

	protected function pingServer($ip){
		$output = trim(shell_exec('ping -c 1 -W 2 '.$ip.' | tail -n 2 | head -n 1'));

		if( !preg_match('/1 received/', $output) ){ return false; }
		return true;
	}
}