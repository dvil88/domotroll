<?php

class logger{
	protected $logLevel;
	protected $file = false;

	public function setFile($file){
		$this->file = fopen($file, 'a');
	}

	public function setLevel($level){
		$this->logLevel = $level;
	}

	public function stdout($msg){
		if(strpos($msg,"\n")){
			$nl = preg_match_all("/\n/",$msg);
			$msg .= ($nl > 1 ? PHP_EOL.'> ': '');
		}elseif($msg[-1] == ']'){
			$msg .= ' ';
		}

		echo "\r\033[1;37m".$msg."\033[0m";
	}

	public function debug($msg){
		$this->showMsg($msg,'DEBUG');
	}
	public function info($msg){
		$this->showMsg($msg,'INFO');
	}
	public function error($msg){
		$this->showMsg($msg,'ERROR');
	}
	public function warn($msg){
		$this->showMsg($msg,'WARNING');
	}
	public function critical($msg){
		$this->showMsg($msg,'CRITICAL');
		exit;
	}
	public function userInput($msg){
		$this->userInputShowMsg($msg);
	}
	
	
	protected function showMsg($msg, $level = false){
		switch($level){
			case 'INFO':$color = '0;32';break;
			case 'DEBUG':$color = '0;34';break;
			case 'PAYLOAD':$color = '0;36';break;
			case 'TRAFFIC OUT':$color = '0;35';break;
			case 'TRAFFIC IN':$color = '45';break;
			case 'ERROR':$color = '0;31';break;
			case 'WARNING':$color = '1;33';break;
			case 'CRITICAL':$color = '41';break;
			default:$color = '1;37';
		}

		if( false && $GLOBALS['config']['misc.disableColoring'] ){
			echo "\r[".date('d-m-Y H:i:s')."] [".$level."] ".$msg.PHP_EOL;
		} else if( $this->file ){
			fwrite($this->file, "\r[".date('d-m-Y H:i:s')."] [".$level."] ".$msg.PHP_EOL);
		} else {
			echo "\r\033[".$color."m[".date('d-m-Y H:i:s')."] [".$level."] ".$msg."\033[0m".PHP_EOL;
		}
	}

	protected function userInputShowMsg($msg){
		echo "\r\033[1m".$msg."\033[0m";
	}
}