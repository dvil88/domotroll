<?php
	class _proc extends _mongo{
		public $table = 'sys.process';
		public $fields = [
			 '_id'=>'INTEGER AUTOINCREMENT'
			,'pid'=>'INTEGER DEFAULT 0'
			,'procLock'=>'TEXT'
			,'procLockMode'=>'TEXT' // shared | exclusive
			,'procStatus'=>'TEXT'

			,'procWorker'=>'TEXT'
			,'procCall'=>'TEXT'
			,'procParams'=>'TEXT'

			,'procDependency'=>'TEXT'
			,'procUser'=>'INTEGER'
			,'procTimeLimit'=>'TEXT'
			,'procProgress'=>'TEXT'
			,'procCurrent'=>'TEXT'
			,'procScheduled'=>'TEXT'
			,'procMsgLines'=>'TEXT'
			,'procTS'=>'TEXT' /* Timestamps about the process */
			,'procFD'=>'TEXT' /* File descriptors of the process */
		];
		public $indexes = [
			 ['fields'=>['procLock'=>1],'props'=>['background'=>true]]
			,['fields'=>['pid'=>1],'props'=>['background'=>true]]
			,['fields'=>['procStatus'=>1],'props'=>['background'=>true]]
		];
		/* END-mongo indexes */

		public function __construct(){
			$this->server = 'mongodb://'.config::$mongoConfig['user'].':'.config::$mongoConfig['pass'].'@'.config::$mongoConfig['host'].':'.config::$mongoConfig['port'];
			$this->db = config::$mongoConfig['database'];
		}
	
		function validate(&$data = [], &$oldData = []){
			if (!isset($data['procStatus'])) {$data['procStatus'] = (isset($data['pid']) && $this->is_running($data['pid'])) ? 'running' : 'awaiting';}
			if (!isset($data['procLockMode'])) {$data['procLockMode'] = 'shared';}
			if (!isset($data['procProgress'])) {$data['procProgress'] = 0;}
			if ($data['procStatus'] == 'scheduled' && !isset($data['procScheduled']['minutes'],$data['procScheduled']['hours'])) {return ['errorDescription'=>'NO_SCHEDULE','file'=>__FILE__,'line'=>__LINE__];}
			if (!in_array($data['procStatus'],['scheduled','scheduled.disabled']) && isset($data['procScheduled'])) {unset($data['procScheduled']);}
			if (empty($data['procWorker'])) {return ['errorDescription'=>'WORKER_ERROR','file'=>__FILE__,'line'=>__LINE__];}
			if ($data['procLockMode'] == 'shared') {$data['procLockMode'] = 'exclusive';}
			if (empty($data['procLock']) && !empty($data['procWorker'])) {$data['procLock'] = $data['procWorker'];}

			/* Si la tarea está pendiente no puede tomar pID aún */
			if ($data['procStatus'] == 'awaiting' && isset($data['pid'])) {unset($data['pid']);}
			if ($data['procStatus'] == 'awaiting' && $data['procLockMode'] == 'scheduled') {$data['procLockMode'] = 'exclusive';}
			if ($data['procStatus'] == 'running') {
				$this->cleanup();
				$data['pid'] = getmypid();
				if( ($oldProcess = $this->getSingle(['pid'=>$data['pid']])) ){
					//FIXME:
					//$r = $this->finished($proc);
				}
				if (!isset($data['procTS']['start'])) {$data['procTS']['start'] = time();}
			}
			if ($data['procStatus'] == 'finished') {
				if (!isset($data['procTS']['end'])) {$data['procTS']['end'] = time();}
			}
			if ($data['procStatus'] == 'scheduled') {
				if (!isset($data['procScheduled']['minutes'])) {$data['procScheduled']['minutes'] = '*';}
				if (!isset($data['procScheduled']['hours'])) {$data['procScheduled']['hours'] = '*';}

				if (is_string($data['procScheduled']['minutes'])) {
					if ($data['procScheduled']['minutes'] === '*') {$data['procScheduled']['minutes'] = range(0,59);}
					elseif (is_numeric($data['procScheduled']['minutes'])) {$data['procScheduled']['minutes'] = [$data['procScheduled']['minutes']];}
					else {$data['procScheduled']['minutes'] = range(0,59,substr($data['procScheduled']['minutes'],strpos($data['procScheduled']['minutes'],'/') + 1));}
				}

				if (is_string($data['procScheduled']['hours'])) {
					if ($data['procScheduled']['hours'] === '*') {$data['procScheduled']['hours'] = range(0,23);}
					elseif (is_numeric($data['procScheduled']['hours'])) {$data['procScheduled']['hours'] = [$data['procScheduled']['hours']];}
					else {$data['procScheduled']['hours'] = range(0,23,substr($data['procScheduled']['hours'],strpos($data['procScheduled']['hours'],'/') + 1));}
				}

				/* procScheduled -> minutes = range(0,59), hours = range(0,23) */
				$next = $this->schedule($data['procScheduled']);
				$data['procScheduled']['next'] = $next;
			}

			if (isset($data['procWorker']) && substr($data['procWorker'],-4) == '.php') {$data['procWorker'] = substr($data['procWorker'],0,-4);}
			if (isset($data['procParams']) && is_string($data['procParams'])) {$data['procParams'] = json_decode($data['procParams'],true);}

			return $data;
		}
		function is_running($pid = ''){
			return file_exists('/proc/'.$pid);
		}
		function cleanup(){
			$daemonOB = $this->getSingle(['procWorker'=>'_daemon','procStatus'=>'running']);
			if ($daemonOB && $this->is_running($daemonOB['pid'])) {
				/* Nos aseguramos que el proceso que está corriendo 
				 * con ese pid es el proceso original que se registró */
				$cmd = str_replace("\0",' ',file_get_contents('/proc/'.$daemonOB['pid'].'/cmdline'));
				if (!preg_match('!php daemon!',$cmd,$m)) {
					$this->finished($daemonOB);
					$daemonOB = false;
				}
			}

			$this->_running_ids = [];
			if (!empty($daemonOB)) {
				$childs = _proc_utils::ps_getChilds($daemonOB['pid']);
				$ids = array_map(function($n){
					$test = preg_match('!php worker [^ ]+ (?<mid>[a-f0-9]{24})!',$n['cmd'],$m);
					return $m['mid'] ?? false;
				},$childs);
				$this->_running_ids = array_filter($ids);
			}

			$this->iterator(['procStatus'=>'running'],function($procOB){
				$mid = strval($procOB['_id']);
				if (empty($procOB['pid'])) {return false;}
				if (!$this->is_running($procOB['pid'])) {
					if (in_array($mid,$this->_running_ids)) {
						/* extraño */
						exit;
					}
					$this->finished($procOB);
					return false;
				}
				if ($procOB['procWorker'] !== '_daemon'
				 && !in_array($mid,$this->_running_ids)) {
					/* Es otro proceso con el mismo pid */
					$this->finished($procOB);
					return false;
				}

				$fd_error = '/proc/'.$procOB['pid'].'/fd/2';
				if (false && ($buffer = file_get_contents($fd_error))) {
					if (strlen($buffer) > 10000) {$buffer = substr($buffer,-10000);}
					
					//FIXME: lo cargamos en el proceso
					$op = [];
					$op['$set']['procFD.error'] = $buffer;
					$r = $this->findAndModify(['_id'=>$procOB['_id']],$op);
				}
			},['fields'=>['pid'=>true,'procWorker'=>true]]);
		}
		function running(&$proc = []){
			$proc['procStatus'] = 'running';
			$proc['pid'] = getmypid();

			$op = [];
			$op['$set']['procStatus'] = $proc['procStatus'];
			$op['$set']['procMsgLines'] = [];
			$op['$set']['procTS.start'] = time();
			$op['$set']['pid'] = $proc['pid'];

			$r = $this->findAndModify(['_id'=>$proc['_id']],$op,['upsert'=>false]);
			return $r;
		}
		function finished(&$proc = []){
			if (!is_array($proc)) {
				var_dump($proc);
				exit;
			}
			$proc['procStatus'] = 'finished';

			$op = [];
			$op['$set']['procStatus'] = $proc['procStatus'];
			$op['$set']['procTS.end'] = time();

			$r = $this->findAndModify(['_id'=>$proc['_id']],$op,['upsert'=>false]);
			return $r;
		}
		function schedule($sch = [],$time = false,$debug = false){
			if (!$time) {$time = time();}
			if( isset($sch['procScheduled']) ){$sch = $sch['procScheduled'];}
			$time = strtotime('+1 minute',$time);
			$hour = $minute = $day = $month = $year = 0;
			$gtime = function($time) use (&$hour,&$minute,&$day,&$month,&$year){
				$hour   = date('G',$time);
				$minute = intval(date('i',$time));
				$day    = intval(date('j',$time));
				$month  = intval(date('n',$time));
				$year   = intval(date('Y',$time));
			};
			$gtime($time);

			//$sch = ['minutes'=>[37],'hours'=>[1,2]];
			if (!isset($sch['hours'],$sch['minutes'])) {return false;}
			if (!is_array($sch['hours']) || !is_array($sch['minutes'])) {return false;}

			$nextHour   = false;
			$nextMinute = false;
			$collition  = false;
			$secure = 10;
			do{
				$stop = true;

				/* INI-Buscamos la hora */
				$hours = $sch['hours'];
				foreach ($hours as $k=>$h) {if( $h < $hour ){unset($hours[$k]);}}
				if (!$hours) {
					/* Si no hay horas, nos vamos hasta las 00:00 del día siguiente
					 * y volvemos a intentar */
					$time = strtotime(date('Y-m-d',strtotime('+1 day',$time)));
					$gtime($time);
					$stop = false;
					continue;
				}
				$nextHour = reset($hours);
				/* END-Buscamos la hora */

				/* INI-Buscamos los minutos */
				$minutes = $sch['minutes'];
				foreach( $minutes as $k=>$m ){if( $m < $minute ){unset($minutes[$k]);}}
				if( !$minutes ){
					/* Si no hay horas, nos vamos hasta las 00:00 del día siguiente
					 * y volvemos a intentar */
					$time = strtotime(date('Y-m-d H:00:00',strtotime('+1 hour',$time)));
					$gtime($time);
					$stop = false;
					continue;
				}
				$nextMinute = reset($minutes);
				/* END-Buscamos los minutos */

				$dateString = $year.'-'
					.str_pad($month,2,'0',STR_PAD_LEFT).'-'
					.str_pad($day,2,'0',STR_PAD_LEFT).' '
					.str_pad($nextHour,2,'0',STR_PAD_LEFT).':'
					.str_pad($nextMinute,2,'0',STR_PAD_LEFT).':'
					.'00';
				
			}while (!$stop && ($secure-- > 0)) ;

			if( $nextHour === false || $nextMinute === false ){return false;}
			$nextTime = strtotime($dateString);
			if( $debug ){var_dump($dateString);exit;}
			return $nextTime;
		}
		function daemon($debug = false){
			/* Registramos el proceso */
			$this->cleanup();
			if (!$debug) {
				$process = [
					 'procLock'=>'_proc.daemon'
					,'procLockMode'=>'exclusive'
					,'procWorker'=>'_daemon'
					,'procStatus'=>'running'
				];
				if ($this->getSingle($process)) {echo 'locked';exit;}
				$this->save($process);
			}

			$this->tasks = [];
			$task = false;
			while (true) {
				$time = time();

				$scheduledClause = ['procStatus'=>'scheduled','procScheduled.next'=>['$lt'=>$time]];
				$scheduledOBs = $this->getWhere($scheduledClause);

				if (isset($scheduledOBs['errorDescription'])) {return $scheduledOBs;}

				foreach ($scheduledOBs as $procOB) {
					/* Locking - evitamos que se lancen 2 procesos incompatibles */
					if (empty($procOB['procLock']) && !empty($procOB['procWorker'])) {$procOB['procLock'] = $procOB['procWorker'];}
					if ($procOB['procLockMode'] == 'exclusive') {
						$test = $this->getSingle(['procStatus'=>['$in'=>['awaiting','running']],'procLock'=>$procOB['procLock']],['fields'=>['_id'=>true]]);
						if (isset($test['errorDescription'])) {return $test;}
						if (!empty($test)) {continue;}
					}

					$procCopy = ['procStatus'=>'awaiting'] + $procOB;
					unset($procCopy['_id']);
					$this->save($procCopy);

					$next = $this->schedule($procOB['procScheduled']);
					$procOB['procScheduled']['next'] = $next;
					$this->save($procOB);

				}

				$awaitingClause = ['procStatus'=>'awaiting'];

				$procOBs = $this->getWhere($awaitingClause);
				if (isset($procOBs['errorDescription'])) {return $procOBs;}
				foreach ($procOBs as $procOB) {
					$id = strval($procOB['_id']);
					$this->tasks[$id] = new _task($procOB);
					$this->tasks[$id]->start();
					sleep(1);
				}
				foreach ($this->tasks as $k=>$task) {
					if (!$task->isRunning()) {
						unset($this->tasks[$k]);
						continue;
					}
				}
				sleep(20);
				$this->cleanup();
			}
		}
		function paintScheduledSchema($params = []){
			$colors = [
				 '#f4584b','#1fc36a','#14b9fb','#9d86d7'
				,'#71ffba','#66b2a8','#351559','#0df0fe'
				,'#dbb9fc','#5d9624','#fffa96','#ab7992','#4d6d9e','#ffa936'
				,'#592140','#ff6619','#f24885','#900093','#3e7e4f','#f78e77'
				,'#a89afc','#a60000','#dc80c4'
			];
			$colorIndex = 0;
			$top = 0;
			$params += [
				 'lapse'=>24
				,'width'=>24*60
				,'cell.height'=>20
				,'cell.width'=>60
				,'legend.width'=>100
			];

			$workerOBs = $this->getWhere(['procStatus'=>'scheduled']);

			/* INI-Leyenda */
			$legend  = '<g class="fragment legend">'.PHP_EOL;
			/* END-Leyenda */

			$startTime = strtotime('-1 minute',strtotime(date('Y-m-d')));
			$today     = date('Y-m-d');

			$svg  = '<svg xmlns="http://www.w3.org/2000/svg" width="'.($params['legend.width']+$params['width']+1).'" height="{%height%}">'.PHP_EOL;
			$left = $params['legend.width'];
			foreach( range(0,24) as $hour ){
				$svg  .= '<rect x="'.$left.'" y="0" width="1" height="100%" style="fill:#eee"></rect>'.PHP_EOL;
				$left += $params['cell.width'];
			}
			foreach( $workerOBs as $workerOB ){
				$svg .= '<rect x="0" y="'.($top).'" width="100%" height="1" style="fill:#eee"></rect>'.PHP_EOL;
				//$svg .= '<rect x="0" y="'.($top).'" width="100%" height="18" style="fill:#fff"></rect>'.PHP_EOL;
				$svg .= '<text x="10" y="'.($top+3).'" height="'.$params['cell.height'].'" style="fill:#555;font-size:10;font-family:arial;dominant-baseline:hanging;">'.$workerOB['procWorker'].'</text>'.PHP_EOL;

				$launchs = [];
				$time = $startTime;
				do{
					$time = $this->schedule($workerOB,$time);
					$day  = date('Y-m-d',$time);
					if( $day == $today && $time && !in_array($day,$launchs) ){$launchs[] = $time;}
				}while( $day == $today );

				$left = $params['legend.width'];
				foreach( $launchs as $k=>$launch ){
					$d = date('H',$launch);
					$m = date('m',$launch);
					$x = $params['legend.width']+($d*$params['cell.width'])+$m;
					$w = 10;

					$svg .= '<g transform="translate(0,'.$top.')">'.PHP_EOL;
					$svg .= '<title>'.date('H:i:s',$launch).'</title>'.PHP_EOL;
					$svg .= '<rect x="'.($x).'" y="1" width="'.$w.'" height="'.($params['cell.height']).'" style="fill:'.$colors[$colorIndex].'"></rect>'.PHP_EOL;
					$svg .= '<rect x="'.($x+1).'" y="2" width="'.($w-2).'" height="'.($params['cell.height']-2).'" style="fill:#fff;fill-opacity:.2;"></rect>'.PHP_EOL;
					$svg .= '</g>'.PHP_EOL;
				}
				$top += $params['cell.height'];
				$colorIndex++;
			}

			$svg .= $legend;
			$svg .= '</svg>';
			$svg = str_replace('{%height%}',$top+1,$svg);
			return $svg;
		}
	}

	class _task{
		public $work    = false;
		public $command = false;
		public $output  = '';
		public $res     = false;
		public $pipes   = [];
		function __construct($work = []){
			$bin_php = '/usr/bin/php';

			$this->work = $work;
			if (isset($this->work['procWorker']) && $this->work['procWorker']) {
				// $this->command = $bin_php.' ../cli/cli.proc.php worker '.$this->work['procWorker'].' '.$this->work['_id'].' > /dev/null';
				$this->command = [
					'exec',
					'/usr/bin/php',
					'../cli/cli.proc.php',
					'worker',
					$this->work['procWorker'],
					$this->work['_id'],
					'> /dev/null'
				];
				$this->command = implode(' ',$this->command);
				// var_dump($this->command);
			}
		}
		function start(){
			$descriptor = [
				 ['pipe','r']
				,['pipe','w']
				,['pipe','w']
			];
			/* Open the resource to execute $command */
			$this->res = proc_open($this->command,$descriptor,$this->pipes);

			/* Set STDOUT and STDERR to non-blocking */
			// stream_set_blocking($this->pipes[1],0);
			// stream_set_blocking($this->pipes[2],0);
			return $this->res; 
		}
		function isRunning(){
			$info = proc_get_status($this->res);

			return $info['running'];
		}
		function listen(){
			while( $r = stream_get_contents($this->pipes[1]) ){$this->output .= $r;}
			return $this->output;
		}
	}

	class _worker{
		public $work    = false;
		public $proc    = false;
		public $lock    = false;
		public $verbose = false;
		public $params  = [];
		public $include = [];
		public $dependencies = [];
		public $observe = []; /* Observe files to stop in case of change */
		public $observe_hash = false; /* Hash representing observed files */
		public $observe_last = false;
		public $update_last = false;
		public $is_root = false;
		function __construct($hash = ''){
			$this->proc   = new _proc();
			$this->work   = $this->proc->getByID($hash);
			$this->params = $this->work['procParams'] ?? [];
			foreach( $this->include as $file ){
				if( file_exists($file) ){include_once($file);}
			}
			if (trim(shell_exec('whoami')) == 'dvil') {
				$this->is_root = true;
				$this->verbose = true;
				ini_set('display_errors',true);
				error_reporting(E_ALL);
			} else {
				/* If not, the process halt until you cat /proc/$pid/fd/1 */
				error_reporting(0);
				ob_start();
			}

			if (!empty($this->dependencies)) {
				/* Generate observe_hash to check if the files get changes
				 * over time to control long running workers and reset them */
				foreach ($this->dependencies as $dependency) {
					$reflector = new ReflectionClass($dependency);
					$this->observe[] = $reflector->getFileName();
				}
				$this->observe_hash = $this->fingerprint();
				$this->observe_last = time();
			}
		}
		function __destruct(){
			$this->clean();
		}
		function task(){
			/* Task the worker should do */
		}
		function clean(){
			/* Cleanup method */
		}
		function start(){
			if (empty($this->work)) {return false;}

			if ($this->is_root) {
				// posix_setuid(33);
				ini_set('display_errors',true);
				error_reporting(E_ALL);
			}
			register_shutdown_function('proc_on_shutdown');


			$this->proc->running($this->work);
			$this->update(0);
			$this->task();
			$this->update(100);
			$this->proc->finished($this->work);
			if ($this->is_root) {
				posix_setuid(0);
			}
		}
		function update($current,$total = false){
			$perc = round($current,2);

			$op = [];
			if ($total
			 && is_numeric($current)
			 && is_numeric($total)) {
				$perc = floatval($current / $total);
				$perc = round($perc * 100,2);
				$this->work['procCurrent'] = $current;
				$op['$set']['procCurrent'] = $current;
			}

			if ($this->update_last !== false
			 && strval($this->update_last) == strval($perc)) {
				/* If the resulting percentage is the same then
				 * avoid the update */
				return false;
			}

			$this->work['procProgress'] = $perc;
			$op['$set']['procProgress'] = $perc;
			$this->proc->findAndModify(['_id'=>$this->work['_id']],$op,['_id'=>true]);
			$this->update_last = $perc;
		}
		function check(){
			if ($this->observe_hash
			 && $this->observe_last < (time() - 10) ) {
				/* Avoid consecutive calls with a timestamp */
				$this->observe_last = time();
				/* If the libraries this worker depend on changes, we stop the worker
				 * to recycle the code of this libs */
				if ($this->fingerprint() != $this->observe_hash) {
					$this->outln('Forced stop to sync libs',time());
					exit;
				}
			}
		}
		function fingerprint(){
			if (!$this->observe_hash) {return false;}
			$md5_libs = [];
			foreach ($this->observe as $lib) {$md5_libs[$lib] = md5_file($lib);}
			return md5(implode($md5_libs));
		}
		function outln($str = '',$timestamp = true){
			if (!isset($this->work['procMsgLines'])) {$this->work['procMsgLines'] = [];}
			if ($timestamp) {$str = '['.date('Y-m-d H:i:s').'] '.$str;}
			if ($this->verbose) {echo $str.PHP_EOL;}
			$op = [];
			$op['$push']['procMsgLines'] = [
				 '$each'=>[$str]
				,'$slice'=>-500
			];
			$r = $this->proc->findAndModify(['_id'=>$this->work['_id']],$op,['_id'=>true]);
			if (isset($r['errorDescription'])) {return $r;}
		}
		function error($error = []){
			$op = [];
			$op['$set']['procError']  = $error;
			$op['$set']['procStatus'] = 'error';
			$r = $this->proc->findAndModify(['_id'=>$this->work['_id']],$op,['_id'=>true]);
			if (isset($r['errorDescription'])) {return $r;}
		}
	}

	class _proc_utils{
		function service_parse($blob = '',$params = []){
			if (!strpos($blob,PHP_EOL)) {
				$blob = shell_exec('/usr/sbin/service '.$blob.' status');
			}
			$r = preg_match('!● (?<name>[^\n]+)\n'
				.'[ ]*Loaded: (?<loaded>[a-z]+) \((?<file>[^;]+); (?<enabled>enabled|disabled);[^\)]*\)\n'
				.'[ ]*Active: (?<status>[a-z]+)[^\n]*\n'
				.'.*?'
				.'[ ]*Main PID: (?<pid>[0-9]+)[^\n]*\n'
				.'!sm',$blob,$m);
			if (!$r) {return false;}
			return [
				 'name'=>$m['name']
				,'loaded'=>$m['loaded'] == 'loaded'
				,'enabled'=>$m['enabled'] == 'enabled'
				,'file'=>$m['file']
				,'status'=>$m['status']
				,'pid'=>$m['pid']
			];
		}
		public static function ps_parse($blob = '',$params = []){
			/* $params = [
			 * 	'user'=>'filtro de usuario'
			 * ]; */
			$lines = explode(PHP_EOL,$blob);

			/* INI-Generamos la regex */
			$header = array_shift($lines);
			preg_match_all('![ ]*(?<fields>[^ ]+)!',$header,$fields);
			if (empty($fields['fields']) || !in_array('USER',$fields['fields'])) {
				/* Necesitamos que venga la cabecera */
				return [];
			}

			$user  = $fields['fields'][0];
			$pid   = $fields['fields'][1];
			$end = array_pop($fields['fields']);
			$regex = '!^(?<'.trim(strtolower($user)).'>[^ ]+)'
				.'(?<'.trim(strtolower($pid)).'>[ ]*[0-9]+)';
			foreach ($fields['fields'] as $k=>$field) {
				$name = str_replace('%','',trim(strtolower($field)));
				if ($name == 'user' || $name == 'pid') {continue;}
				$regex .= '(?<'.$name.'>.{'.strlen($fields[0][$k]).'})';
			}
			$regex .= '(?<'.trim(strtolower($end)).'>.*)$!';
			/* END-Generamos la regex */

			$rows = [];
			foreach ($lines as $line) {
				if (!preg_match($regex,$line,$data)) {continue;}
				foreach ($data as $k=>$v) {
					if (preg_match('!^[0-9]+!',$k)) {unset($data[$k]);continue;}
					$data[$k] = trim($v);
				}
				if (isset($params['user']) && $params['user'] != $data['user']) {continue;}
				$rows[$data['pid']] = $data;
			}
			return $rows;
		}
		public static function ps_getChilds(int $pid) : array{
			$childs = [];
			$_path = new _path('/proc/');
			$_path->iterator('*',function($_file) use ($pid,&$childs){
				if (in_array($_file,['/proc/self','/proc/thread-self'])) {return false;}
				if (!file_exists($_file.'/status')) {return false;}
				$status = @file_get_contents($_file.'/status');
				if (empty($status)) {return false;}
				$status = preg_match_all('!(?<name>[a-zA-Z]+):[ \t]*(?<value>[^\n]+)\n!',$status,$m);
				$info   = array_combine($m['name'],$m['value']);
				$info['PPid'] = intval($info['PPid']);
				if ($info['PPid'] !== $pid) {return false;}

				$cmd = str_replace("\0",' ',file_get_contents('/proc/'.$info['Pid'].'/cmdline'));
				$childs[$info['Pid']] = [
					 'name'=>$info['Name']
					,'pid'=>intval($info['Pid'])
					,'cmd'=>$cmd
				];
			});

			return $childs;
		}
		function isRunning($pid){
			return file_exists('/proc/'.$pid);
		}
	}

	function proc_on_shutdown(){
		$proc = new _proc();
		$pid  = getmypid();
		if( ($procOBs = $proc->getWhere(['pid'=>$pid])) ){foreach( $procOBs as $procOB ){
			$r = $proc->finished($procOB);
		}}
		return true;
	}

	class proctime{
		public $time = '';
		public function __construct(){
			$this->time = time();
		}
		public function isBetween($down = '',$up = ''){
			$strtotime = function($str){
				if( preg_match('/[0-9]{2}:[0-9]{2}/',$str) ){return strtotime(date('Y-m-d ').$str);}
			};

			if( $this->time > $strtotime($down) && $this->time < $strtotime($up) ){return true;}
			return false;
		}
	}


