<?php
	class _api{
		public $_preffix = '';
		public $_path    = '';
		public $_input   = '';
		public $_method  = '';
		public $_last    = '';
		public $_page    = 1;
		public $_chunks  = [];
		public $_op      = false;
		public $_graph   = [];
		public $_graph_append = false;
		public $_search_condition = '';
		public $_search_literals = [];
		function __construct(){
			$uri = $_SERVER['REQUEST_URI'] ?? '/';
			if (!empty($this->_preffix)) {
				$uri = preg_replace('!^'.$this->_preffix.'!','',$uri);
			}

			$uri = parse_url($uri);
			$this->_path = explode('/',$uri['path'] ?? '');
			$this->_path = array_filter($this->_path);

			$this->_input = file_get_contents('php://input');
			if (empty($this->_input) && !empty($_SERVER['REQUEST_INPUT'])) {$this->_input = $_SERVER['REQUEST_INPUT'];}
			if (empty($this->_input)) {$this->_input = [];}
			if (!empty($this->_input) && is_string($this->_input)) {$this->_input = json_decode($this->_input,true);}
			$this->_method = $_SERVER['REQUEST_METHOD'] ?? 'GET';
			$this->_page = 1;
			$this->_items = 100;
			if (!empty($_GET['page']) && preg_match('/^[0-9]+$/',$_GET['page']) && $_GET['page'] > 1) {
				$this->_page = intval($_GET['page']);
			}
			if (!empty($_GET['items']) && preg_match('/^[0-9]+$/',$_GET['items']) && $_GET['items'] > 1) {
				$this->_items = intval($_GET['items']);
			}

			if (!empty($_SERVER['HTTP_GRAPH'])
			 && ($_SERVER['HTTP_GRAPH'][0] == '{' || substr($_SERVER['HTTP_GRAPH'],0,2) == '+{')
			 && substr($_SERVER['HTTP_GRAPH'],-1) == '}') {
				$this->_graph = $this->_parse_graph($_SERVER['HTTP_GRAPH']);
			}

			$this->_chunks = array_chunk($this->_path,2);
			$this->_path   = [];
			$this->_op = false;
			$keys = array_keys($this->_chunks);
			$last = end($keys);

			foreach ($this->_chunks as $k=>$chunk) {
				if (count($chunk) < 2) {$chunk[] = '';}
				list($method,$id) = $chunk;
				
				if ($method[0] == '_') {
					$this->_error('E_NOT_ALLOWED');
				}
				if (in_array($method,$this->_path)) {
					$this->_error('E_CIRCULAR');
				}

				if (!method_exists($this,$method)) { break;}
				$this->_op = $method;
				if ($k !== $last) {
					$this->{$this->_op}($id);
					$this->_last = $this->_op;
					$this->_path[] = $this->_op;
				}
			}

			if (!empty($this->_op)) {
				$this->{$this->_op}($id,$this->_method);
			}
			exit;
		}
		function _error($e = ''){
			header('Content-type: application/json');
			echo '{"status":"KO","error":"'.$e.'"}';exit;
		}
		function _result($r = [],$m = []){
			if (is_array($r)) {
				if (!isset($r['_id'])) {
					foreach ($r as &$row) {
						if (!is_array($row)) {continue;}
						if (false && isset($row['_id'])
						 && get_class($row['_id']) == 'MongoDB\BSON\ObjectId') {$row['_id'] = strval($row['_id']);}
					}
					unset($row);
				}
			}
			$meta = ',"meta":'.json_encode($m);
			if (empty($r)) {$r = json_decode('{}');}
			header('Content-type: application/json');
			echo '{"status":"OK","result":'.json_encode($r).(!empty($m) ? $meta : '').'}';exit;
		}
		function _require(...$args){
			foreach ($args as $step) {
				if (!in_array($step,$this->_path)) {
					$this->_error('E_NOT_ALLOWED');
				}
			}
		}
		function _require_test(...$args){
			/* Vamos a comprobar si los últimos paths recorridos 
			 * coinciden con los argumentos de la función */
			$strn = implode('.',array_reverse($args));
			$strh = implode('.',array_reverse($this->_path));
			if (strpos($strh,$strn) === 0) {return true;}
			return false;
		}
		function _valid_input(string $_struct){
			do {$_struct = preg_replace('/([\{,]{1})([_a-zA-Z]+)([\},:]{1})/','$1"$2"$3',$_struct,-1,$n);} while ($n > 0);
			$_struct = preg_replace('/\"([\},]{1})/','":true$1',$_struct);
			$_struct = json_decode($_struct,true);
			if ($_struct === null) {return false;}

			$this->_valid_input_segment($this->_input,$_struct);
		}
		function _valid_input_segment(array &$_input,array &$_struct){
			foreach ($_input as $_key=>$_value) {
				if (!isset($_struct[$_key])) {
					unset($_input[$_key]);
					continue;
				}

				if (is_array($_struct[$_key])) {
					$this->_valid_input_segment($_input[$_key],$_struct[$_key]);
				}
			}
		}
		function _parse_graph($_struct){
			$this->_graph_append = false;
			if ($_struct[0] == '+') {
				$this->_graph_append = true;
				$_struct = substr($_struct,1);
			}
			do {$_struct = preg_replace('/([\{,]{1})([a-zA-Z]+)([\},:]{1})/','$1"$2"$3',$_struct,-1,$n);} while ($n > 0);
			$_struct = preg_replace('/\"([\},]{1})/','":true$1',$_struct);
			$_graph  = json_decode($_struct,true);
			return $_graph ? $_graph : [];
		}
		function _search(string $field){
			$this->_search_condition = [];
			$this->_search_literals = [];
			if (!empty($_GET['search']) && is_string($_GET['search'])) {
				$_GET['search'] = preg_replace_callback('/\"(?<word>[^\"]+)\"/',function($m){
					$this->_search_literals[] = $m['word'];
					return '';
				},$_GET['search']);

				$words = explode(' ',$_GET['search']);
				$words = array_unique(array_diff($words,['-']));
				$words = array_merge($this->_search_literals,$words);
				foreach ($words as $word) {
					if (empty($word)) {continue;}
					if (strlen($word) < 2) {continue;}
					if (strlen($word) == 24 && preg_match('/^[a-f0-9]{24}$/',$word)) {
						$this->_search_condition[] = ['_id'=>new MongoDB\BSON\ObjectId($word)];
					}
					$this->_search_condition[] = [$field=>['$regex'=>preg_quote($word),'$options'=>'i']];
				}
			}
		}
	}
