<?php

class swoole{
	protected $stats;
	protected static $timers = [];

	public function __construct(){
		$this->stats = new stats();
	}

	public static function onStart(Swoole\WebSocket\Server $server) {
		echo "Swoole WebSocket Server is started at ws://{$server->host}:{$server->port}\n";
		// print_r($server);
	}

	public static function onOpen(Swoole\WebSocket\Server $server, Swoole\Http\Request $request){
		echo "connection open: {$request->fd}\n";
		// print_r($request);

		$timerId = $server->tick(5000, function() use ($server, $request){
			(new self)->sendStats($server, $request);
		});

		echo 'new timer: '.$timerId.PHP_EOL;
		self::$timers[$request->fd] = $timerId;


		/*
		$server->tick(1000, function() use ($server, $request) {
			$server->push($request->fd, json_encode(["hello", time()]));
		});
		*/
	}

	public static function onMessage(Swoole\WebSocket\Server $server, Swoole\WebSocket\Frame $frame) {
		echo "received message: {$frame->data}\n";
		// $server->push($frame->fd, json_encode(json_encode(['ACK', time()])));

		self::readMessage($server, $frame);
	}

	public static function onClose(Swoole\WebSocket\Server $server, int $fd) {
		echo "connection close: {$fd}\n";

		echo 'Clear timer '.$fd.' => '.self::$timers[$fd].PHP_EOL;
		$server->clearTimer(self::$timers[$fd]);
		unset(self::$timers[$fd]);
	}

	protected function readMessage($server, $frame){
		$message = json_decode($frame->data, 1);
		switch( $message[0] ){
			case 'ACK':
				break;
			case 'ping':
				$server->push($frame->fd, json_encode(['pong', time()]));
				break;
		}
	}

	protected function sendStats($server, $request){
		$st = [
			'cpu'=>$this->stats->getCPU(),
			'mem'=>$this->stats->getMemory(),
			'disks'=>$this->stats->getDisks(),
			'load'=>$this->stats->getLoad(),
		];

		$server->push($request->fd, json_encode(['stats', $st]));
	}

}