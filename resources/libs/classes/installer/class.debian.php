<?php

class debian{
	private $ssh2;
	private $rootPassword;
	private $sudo = '';
	
	public $neededPackages = [
		'git',
		'php7.3',
		'php7.3-bcmath',
		'php7.3-cgi',
		'php7.3-cli',
		'php7.3-curl',
		'php7.3-dev',
		'php7.3-fpm',
		'php7.3-gd',
		'php7.3-ldap',
		'php7.3-mbstring',
		'php7.3-mysql',
		'php-pear',
		'php7.3-xml',
		'php7.3-zip',
		'php-mongodb',
		'php-redis',
	];
	protected $packagesToInstall = [];

	public function __construct($ssh2, $rootPassword){
		$this->ssh2 = $ssh2;
		$this->rootPassword = $rootPassword;

		$this->installer = 'apt-get';
		
		// Check if sudo is needed
		$isRoot = $this->ssh2->command('if [ "$(whoami)" = "root" ]; then echo 1; else echo 0; fi');
		if( !$isRoot ){
			$this->sudo = 'echo "'.$this->rootPassword.'" | sudo -S ';
		}
	}

	public function command($command){
		return $this->ssh2->command($this->sudo.$command);
	}

	public function getWebServer(){
		// Check if lsof is installed
		$lsof = $this->checkPackage('lsof');
		if( !$lsof ){ $this->installPackage('lsof'); }

		$webServer = $this->ssh2->command($this->sudo.'lsof -i -P -n | grep --color=never "\*:80 " | awk \'{print $1}\' | awk -F, \'!seen[$1]++\'');
		if( !$webServer ){
			$this->packagesToInstall[] = 'nginx';
		}

		return $webServer;
	}

	public function installPHPRepository(){
		$r = $this->ssh2->command('grep -r --include \'*.list\' \'^deb \' /etc/apt/sources.list /etc/apt/sources.list.d/');
		if( !preg_match('!https://packages.sury.org/php/!',$r) ){
			// No tiene instalado la DPA de ondrej 
			$this->ssh2->command($this->sudo.'apt-get -y install apt-transport-https lsb-release ca-certificates curl');
			$this->ssh2->command($this->sudo.'wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg');
			$this->ssh2->command($this->sudo.'sh -c \'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list\'');
			$this->ssh2->command($this->sudo.'apt-get update');
		}
	}

	public function checkPackages(){
		foreach( $this->neededPackages as $package ){
			$packageInstalled = $this->checkPackage($package);
			if( !$packageInstalled ){
				$this->packagesToInstall[] = $package;
			}
		}
	}

	protected function checkPackage($package){
		$cmd = 'dpkg-query -W -f=\'${Status}\n\' '.$package.' 2>&1';
			
		$packageInstalled = $this->ssh2->command($cmd);
		if( !preg_match('/install ok/i', $packageInstalled) ){
			return false;
		}

		return true;
	}

	public function installPackages(){
		$this->installPackage(implode(' ', $this->packagesToInstall));
	}

	protected function installPackage($package){
		$cmd = $this->sudo.$this->installer.' install -y '.$package;
		$this->ssh2->command($cmd);
	}

	public function configureVhostNginx($host, $path){
		$fpmSocket = '/run/php/php7.3-fpm.sock';
		$vhostPath = '/etc/nginx/sites-available/';
		$vhostPathEnabled = '/etc/nginx/sites-enabled/';

		$vhostData = file_get_contents('../db/install/nginx.vhost');
		$vhostData = str_replace(['{{server_name}}', '{{path}}', '{{fpm_socket}}'], [$host, $path, $fpmSocket], $vhostData);

		$this->ssh2->file_put('/tmp/'.$host.'.vhost', $vhostData);
		$this->ssh2->command($this->sudo.'cp /tmp/'.$host.'.vhost '.$vhostPath.$host.'.vhost && rm -rf /tmp/'.$host.'.vhost');
		$this->ssh2->command($this->sudo.'ln -s '.$vhostPath.$host.'.vhost '.$vhostPathEnabled.$host.'.vhost');

		// Replace fastcgi conf
		$this->ssh2->file_send('../db/install/fastcgi-php.conf', '/tmp/fastcgi-php.conf');
		$this->ssh2->command($this->sudo.'cp /tmp/fastcgi-php.conf /etc/nginx/snippets/fastcgi-php.conf && rm -rf /tmp/fastcgi-php.conf');


		$this->ssh2->command($this->sudo.'service nginx reload');
	}

	public function configureVhostApache($host, $path){
		$vhostPath = '/etc/apache2/sites-available/';
		$vhostPathEnabled = '/etc/apache2/sites-enabled/';

		$vhostData = file_get_contents('../db/install/apache.vhost');
		$vhostData = str_replace(['{{server_name}}', '{{path}}'], [$host, $path], $vhostData);

		$this->ssh2->file_put('/tmp/'.$host.'.vhost', $vhostData);
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S cp /tmp/'.$host.'.vhost '.$vhostPath.$host.'.vhost');
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S ln -s '.$vhostPath.$host.'.vhost '.$vhostPathEnabled.$host.'.vhost');
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S service apache2 reload');
	}

	public function configureVhostHttpd($host, $path){
		$vhostPath = '/etc/httpd/sites-available/';
		$vhostPathEnabled = '/etc/httpd/sites-enabled/';

		$vhostData = file_get_contents('../db/install/apache.vhost');
		$vhostData = str_replace(['{{server_name}}', '{{path}}'], [$host, $path], $vhostData);

		$this->ssh2->file_put('/tmp/'.$host.'.vhost', $vhostData);
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S cp /tmp/'.$host.'.vhost '.$vhostPath.$host.'.vhost');
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S ln -s '.$vhostPath.$host.'.vhost '.$vhostPathEnabled.$host.'.vhost');
		$this->ssh2->command('echo "'.$this->rootPassword.'" | sudo -S service apache2 reload');
	}
}