<?php

class centos{
	private $ssh2;
	private $rootPassword;
	private $sudo = '';

	protected $centosVersion;
	public $neededPackages = [
		'git',
		'php',
		'php-bcmath',
		// 'php-cgi',
		'php-cli',
		'php-curl',
		'php-fpm',
		'php-gd',
		'php-ldap',
		'php-mbstring',
		'php-mysqlnd',
		'php-xml',
		'php-zip',
		'php73-php-phpiredis',

	];
	protected $packagesToInstall = [];

	public function __construct($ssh2, $rootPassword){
		$this->ssh2 = $ssh2;
		$this->rootPassword = $rootPassword;

		$this->installer = 'yum';

		// Get centos version
		$this->centosVersion = $this->ssh2->command('rpm -E %{rhel}');
		
		// Check if sudo is needed
		$isRoot = $this->ssh2->command('if [ "$(whoami)" = "root" ]; then echo 1; else echo 0; fi');
		if( !$isRoot ){
			$this->sudo = 'echo "'.$this->rootPassword.'" | sudo -S ';
		}
	}

	public function command($command){
		return $this->ssh2->command($this->sudo.$command);
	}

	public function getWebServer(){
		// Check if lsof is installed
		$lsof = $this->checkPackage('lsof');
		if( !$lsof ){ $this->installPackage('lsof'); }

		$webServer = $this->ssh2->command($this->sudo.'lsof -i -P -n | grep --color=never "\*:80 " | awk \'{print $1}\' | awk -F, \'!seen[$1]++\'');
		if( !$webServer ){
			$this->packagesToInstall[] = 'nginx';
		}

		return $webServer;
	}

	public function installPHPRepository(){
		$r = $this->ssh2->command('rpm -qa | grep --color=never -i epel');
		if( !preg_match('!epel-release!',$r) ){
			if( $this->centosVersion == 7 ){
				$this->ssh2->command($this->sudo.'yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm');
				$this->ssh2->command($this->sudo.'yum -y install epel-release yum-utils');
				$this->ssh2->command($this->sudo.'yum-config-manager --disable remi-php54');
				$this->ssh2->command($this->sudo.'yum-config-manager --enabl remi-php73');
			} else if( $this->centosVersion == 8 ){
				$this->ssh2->command($this->sudo.'rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm');
				$this->ssh2->command($this->sudo.'yum install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm');
				$this->ssh2->command($this->sudo.'dnf module enable php:remi-7.3 -y');
			}
		}
	}

	public function checkPackages(){
		foreach( $this->neededPackages as $package ){
			$packageInstalled = $this->checkPackage($package);
			if( !$packageInstalled ){
				$this->packagesToInstall[] = $package;
			}
		}
	}

	protected function checkPackage($package){
		$cmd = 'if rpm -q '.$package.' > /dev/null 2>&1; then echo 1; else echo 0; fi';

		$packageInstalled = $this->ssh2->command($cmd);
		if( !$packageInstalled ){ return false; }

		return true;
	}

	public function installPackages(){
		// $this->installPackage(implode(' ', $this->packagesToInstall));
		foreach( $this->packagesToInstall as $package ){
			$this->installPackage($package);
		}


		if( in_array('nginx', $this->packagesToInstall) ){
			$this->ssh2->command($this->sudo.'systemctl enable nginx');
			$this->ssh2->command($this->sudo.'systemctl restart nginx');
		}

		$this->installPHPMongo();
	}

	protected function installPHPMongo(){
		if( $this->centosVersion == 7 ){
		} else if( $this->centosVersion == 8 ){
			$this->ssh2->command($this->sudo.'dnf config-manager --set-enabled PowerTools');
		}


		foreach( ['gcc','make','php-pear','php-devel'] as $package ){
			if( !$this->checkPackage($package) ){
				$this->installPackage($package);
			}
		}

		$mongoInstalled = $this->ssh2->command('if pecl list | grep --color=never -i mongo > /dev/null; then echo 1; else echo 0; fi');
		if( !$mongoInstalled ){
			$this->ssh2->command($this->sudo.'pecl install mongodb');
		}

		// Check if enabled
		$mongoEnabled = $this->ssh2->command('if php -m | grep --color=never -i mongo > /dev/null; then echo 1; else echo 0; fi');
		if( !$mongoEnabled ){
			// Activate mongodb
			$this->ssh2->file_put('/tmp/20-mongodb.ini', 'extension=mongodb.so');
			$this->ssh2->command($this->sudo.'cp /tmp/20-mongodb.ini /etc/php.d/20-mongodb.ini');
			$this->ssh2->command($this->sudo.'systemctl restart php-fpm');
		}
	}

	protected function installPackage($package){
		$cmd = $this->sudo.$this->installer.' install -y '.$package;
		$result = $this->ssh2->command($cmd);
	}

	public function configureVhostNginx($host, $path){
		$fpmSocket = '/run/php-fpm/www.sock';
		$vhostPath = '/etc/nginx/sites-available/';
		$vhostPathEnabled = '/etc/nginx/sites-enabled/';

		// Check if folders exists
		$availableExists = $this->ssh2->command('if [ -d "'.$vhostPath.'" ]; then echo 1; else echo 0; fi');
		if( !$availableExists ){
			$this->ssh2->command('if [ ! -d "'.$vhostPath.'" ]; then '.$this->sudo.'mkdir -p '.$vhostPath.'; fi;if [ -d "'.$vhostPath.'" ]; then echo 1;fi');
		}

		$enabledExists = $this->ssh2->command('if [ -d "'.$vhostPathEnabled.'" ]; then echo 1; else echo 0; fi');
		if( !$enabledExists ){
			$this->ssh2->command('if [ ! -d "'.$vhostPathEnabled.'" ]; then '.$this->sudo.'mkdir -p '.$vhostPathEnabled.'; fi;if [ -d "'.$vhostPathEnabled.'" ]; then echo 1;fi');
		}


		$vhostData = file_get_contents('../db/install/nginx.vhost');
		$vhostData = str_replace(['{{server_name}}', '{{path}}', '{{fpm_socket}}'], [$host, $path, $fpmSocket], $vhostData);

		$this->ssh2->file_put('/tmp/'.$host.'.vhost', $vhostData);
		$this->ssh2->command($this->sudo.'cp /tmp/'.$host.'.vhost '.$vhostPath.$host.'.vhost && rm -rf /tmp/'.$host.'.vhost');
		$this->ssh2->command($this->sudo.'ln -s '.$vhostPath.$host.'.vhost '.$vhostPathEnabled.$host.'.vhost');


		$this->ssh2->command($this->sudo.'sed  \'/http {/a include /etc/nginx/sites-enabled/*;\' /etc/nginx/nginx.conf > /tmp/nginx.conf');
		$this->ssh2->command($this->sudo.'mv /tmp/nginx.conf /etc/nginx/nginx.conf');


		// Replace fastcgi conf
		$snippetsExists = $this->ssh2->command('if [ -d "/etc/nginx/snippets" ]; then echo 1; else echo 0; fi');
		if( !$snippetsExists ){
			$this->ssh2->command('if [ ! -d "/etc/nginx/snippets" ]; then '.$this->sudo.'mkdir -p /etc/nginx/snippets; fi;if [ -d "/etc/nginx/snippets" ]; then echo 1;fi');
		}
		$this->ssh2->file_send('../db/install/fastcgi-php.conf', '/tmp/fastcgi-php.conf');
		$this->ssh2->command($this->sudo.'cp /tmp/fastcgi-php.conf /etc/nginx/snippets/fastcgi-php.conf && rm -rf /tmp/fastcgi-php.conf');


		$this->ssh2->command($this->sudo.'systemctl enable nginx');
		$this->ssh2->command($this->sudo.'systemctl restart nginx');


		$this->selinuxDirectives();
	}

	public function configureVhostHttpd($host, $path){
		$vhostPath = '/etc/httpd/sites-available/';
		$vhostPathEnabled = '/etc/httpd/sites-enabled/';

		// Check if folders exists
		$availableExists = $this->ssh2->command('if [ -d "'.$vhostPath.'" ]; then echo 1; else echo 0; fi');
		if( !$availableExists ){
			$this->ssh2->command('if [ ! -d "'.$vhostPath.'" ]; then '.$this->sudo.'mkdir -p '.$vhostPath.'; fi;if [ -d "'.$vhostPath.'" ]; then echo 1;fi');
		}

		$enabledExists = $this->ssh2->command('if [ -d "'.$vhostPathEnabled.'" ]; then echo 1; else echo 0; fi');
		if( !$enabledExists ){
			$this->ssh2->command('if [ ! -d "'.$vhostPathEnabled.'" ]; then '.$this->sudo.'mkdir -p '.$vhostPathEnabled.'; fi;if [ -d "'.$vhostPathEnabled.'" ]; then echo 1;fi');
		}


		$vhostData = file_get_contents('../db/install/apache.vhost');
		$vhostData = str_replace(['{{server_name}}', '{{path}}'], [$host, $path], $vhostData);

		$this->ssh2->file_put('/tmp/'.$host.'.vhost', $vhostData);
		$this->ssh2->command($this->sudo.'cp /tmp/'.$host.'.vhost '.$vhostPath.$host.'.vhost');
		$this->ssh2->command($this->sudo.'ln -s '.$vhostPath.$host.'.vhost '.$vhostPathEnabled.$host.'.vhost');


		// Add config
		$this->ssh2->command($this->sudo.'sh -c \'printf "\n\n# Load config files in the "/etc/httpd/sites-enabled" directory, if any.\nInclude sites-enabled/*\n" >> /etc/httpd/conf/httpd.conf\'');

		$this->ssh2->command($this->sudo.'systemctl enable httpd');
		$this->ssh2->command($this->sudo.'systemctl restart httpd');


		$this->selinuxDirectives();
	}

	protected function selinuxDirectives(){
		// Check firewall
		$firewall = $this->ssh2->command('if firewall-cmd --zone=public --list-all | grep --color=never -i http > /dev/null; then echo 1; else echo 0; fi');
		if( !$firewall ){
			$this->ssh2->command($this->sudo.'firewall-cmd --zone=public --add-service=http --permanent');
			$this->ssh2->command($this->sudo.'firewall-cmd --zone=public --add-service=http');
		}
		$firewall = $this->ssh2->command('if firewall-cmd --zone=public --list-all | grep --color=never -i https > /dev/null; then echo 1; else echo 0; fi');
		if( !$firewall ){
			$this->ssh2->command($this->sudo.'firewall-cmd --zone=public --add-service=https --permanent');
			$this->ssh2->command($this->sudo.'firewall-cmd --zone=public --add-service=https');
		}

		// Activate mongo in selinux
		$mongoAllowed = $this->ssh2->command('if [ "$(getsebool -a | grep --color=never httpd_can_network_connect_db | awk \'{print $3}\')" = "on" ]; then echo 1; else echo 0; fi');
		if( !$mongoAllowed ){
			$this->ssh2->command($this->sudo.'setsebool -P httpd_can_network_connect_db 1');
		}

		// Check selinux ldap
		$ldapAllowed = $this->ssh2->command('if [ "$(getsebool -a | grep --color=never httpd_can_connect_ldap | awk \'{print $3}\')" = "on" ]; then echo 1; else echo 0; fi');
		if( !$ldapAllowed ){
			$this->ssh2->command($this->sudo.'setsebool -P httpd_can_connect_ldap 1');
		}
	}
}