<?php
class ImageHash{
	/**
	 * Return hashes as hexadecimals.
	 */
	const HEXADECIMAL = 'hex';

	/**
	 * Return hashes as decimals.
	 */
	const DECIMAL = 'dec';

	/**
	 * The hashing implementation.
	 *
	 * @var Implementation
	 */
	protected $implementation;

	/**
	 * @var string
	 */
	protected $mode;

	/**
	 * Constructor.
	 *
	 * @param Implementation $implementation
	 * @param string         $mode
	 */
	public function __construct(Implementation $implementation = null, $mode = self::HEXADECIMAL){
		$this->implementation = $implementation ?: new DifferenceHash;
		$this->mode = $mode;
	}

	/**
	 * Calculate a perceptual hash of an image file.
	 *
	 * @param  mixed $resource GD2 resource or filename
	 * @return int
	 */
	public function hash($resource){
		$destroy = false;

		if (! is_resource($resource)) {
			$resource = $this->loadImageResource($resource);
			$destroy = true;
		}

		$hash = $this->implementation->hash($resource);

		if ($destroy) {
			$this->destroyResource($resource);
		}

		return $this->formatHash($hash);
	}

	/**
	 * Calculate a perceptual hash of an image string.
	 *
	 * @param  mixed $data Image data
	 * @return string
	 */
	public function hashFromString($data){
		$resource = $this->createResource($data);

		$hash = $this->implementation->hash($resource);

		$this->destroyResource($resource);

		return $this->formatHash($hash);
	}

	/**
	 * Compare 2 images and get the hamming distance.
	 *
	 * @param  mixed $resource1
	 * @param  mixed $resource2
	 * @return int
	 */
	public function compare($resource1, $resource2){
		$hash1 = $this->hash($resource1);
		$hash2 = $this->hash($resource2);

		return $this->distance($hash1, $hash2);
	}

	/**
	 * Calculate the Hamming Distance.
	 *
	 * @param int $hash1
	 * @param int $hash2
	 * @return int
	 */
	public function distance($hash1, $hash2){
		if (extension_loaded('gmp')) {
			if ($this->mode === self::HEXADECIMAL) {
				$dh = gmp_hamdist('0x'.$hash1, '0x'.$hash2);
			} else {
				$dh = gmp_hamdist($hash1, $hash2);
			}
		} else {
			if ($this->mode === self::HEXADECIMAL) {
				$hash1 = $this->hexdec($hash1);
				$hash2 = $this->hexdec($hash2);
			}

			$dh = 0;
			for ($i = 0; $i < 64; $i++) {
				$k = (1 << $i);
				if (($hash1 & $k) !== ($hash2 & $k)) {
					$dh++;
				}
			}
		}

		return $dh;
	}

	/**
	 * Convert hexadecimal to signed decimal.
	 *
	 * @param string $hex
	 * @return int
	 */
	public function hexdec($hex){
		if (strlen($hex) == 16 && hexdec($hex[0]) > 8) {
			list($higher, $lower) = array_values(unpack('N2', hex2bin($hex)));
			return $higher << 32 | $lower;
		}

		return hexdec($hex);
	}

	/**
	 * Get a GD2 resource from file.
	 *
	 * @param  string $file
	 * @return resource
	 */
	protected function loadImageResource($file){
		try {
			return $this->createResource(file_get_contents($file));
		} catch (Exception $e) {
			throw new InvalidArgumentException("Unable to load file: $file");
		}
	}

	/**
	 * Get a GD2 resource from string.
	 *
	 * @param string $data
	 * @return resource
	 */
	protected function createResource($data){
		try {
			return imagecreatefromstring($data);
		} catch (Exception $e) {
			throw new InvalidArgumentException('Unable to create GD2 resource');
		}
	}

	/**
	 * Destroy GD2 resource.
	 *
	 * @param resource $resource
	 */
	protected function destroyResource($resource){
		imagedestroy($resource);
	}

	/**
	 * Format hash in hex.
	 *
	 * @param int $hash
	 * @return string|int
	 */
	protected function formatHash($hash){
		return $this->mode === static::HEXADECIMAL ? str_pad(dechex($hash), 16, '0', STR_PAD_LEFT) : $hash;
	}
}

interface Implementation{
	/**
	 * Calculate the hash for the given resource.
	 *
	 * @param  resource $resource
	 * @return int
	 */
	public function hash($resource);
}

class AverageHash implements Implementation{
	const SIZE = 8;

	/**
	 * {@inheritDoc}
	 */
	public function hash($resource){
		// Resize the image.
		$resized = imagecreatetruecolor(static::SIZE, static::SIZE);
		imagecopyresampled($resized, $resource, 0, 0, 0, 0, static::SIZE, static::SIZE, imagesx($resource), imagesy($resource));

		// Create an array of greyscale pixel values.
		$pixels = [];
		for ($y = 0; $y < static::SIZE; $y++) {
			for ($x = 0; $x < static::SIZE; $x++) {
				$rgb = imagecolorsforindex($resized, imagecolorat($resized, $x, $y));
				$pixels[] = floor(($rgb['red'] + $rgb['green'] + $rgb['blue']) / 3);
			}
		}

		// Free up memory.
		imagedestroy($resized);

		// Get the average pixel value.
		$average = floor(array_sum($pixels) / count($pixels));

		// Each hash bit is set based on whether the current pixels value is above or below the average.
		$hash = 0;
		$one = 1;
		foreach ($pixels as $pixel) {
			if ($pixel > $average) {
				$hash |= $one;
			}
			$one = $one << 1;
		}

		return $hash;
	}
}

class DifferenceHash implements Implementation{
	const SIZE = 8;

	/**
	 * {@inheritDoc}
	 */
	public function hash($resource){
		// For this implementation we create a 8x9 image.
		$width = static::SIZE + 1;
		$heigth = static::SIZE;

		// Resize the image.
		$resized = imagecreatetruecolor($width, $heigth);
		imagecopyresampled($resized, $resource, 0, 0, 0, 0, $width, $heigth, imagesx($resource), imagesy($resource));

		$hash = 0;
		$one = 1;
		for ($y = 0; $y < $heigth; $y++) {
			// Get the pixel value for the leftmost pixel.
			$rgb = imagecolorsforindex($resized, imagecolorat($resized, 0, $y));
			$left = floor(($rgb['red'] + $rgb['green'] + $rgb['blue']) / 3);

			for ($x = 1; $x < $width; $x++) {
				// Get the pixel value for each pixel starting from position 1.
				$rgb = imagecolorsforindex($resized, imagecolorat($resized, $x, $y));
				$right = floor(($rgb['red'] + $rgb['green'] + $rgb['blue']) / 3);

				// Each hash bit is set based on whether the left pixel is brighter than the right pixel.
				// http://www.hackerfactor.com/blog/index.php?/archives/529-Kind-of-Like-That.html
				if ($left > $right) {
					$hash |= $one;
				}

				// Prepare the next loop.
				$left = $right;
				$one = $one << 1;
			}
		}

		// Free up memory.
		imagedestroy($resized);

		return $hash;
	}
}

class PerceptualHash implements Implementation{
	const SIZE = 64;

	/**
	 * {@inheritDoc}
	 */
	public function hash($resource){
		// Resize the image.
		$resized = imagecreatetruecolor(static::SIZE, static::SIZE);
		imagecopyresampled($resized, $resource, 0, 0, 0, 0, static::SIZE, static::SIZE, imagesx($resource), imagesy($resource));

		// Get luma value (YCbCr) from RGB colors and calculate the DCT for each row.
		$matrix = [];
		$row = [];
		$rows = [];
		$col = [];
		$cols = [];
		for ($y = 0; $y < static::SIZE; $y++) {
			for ($x = 0; $x < static::SIZE; $x++) {
				$rgb = imagecolorsforindex($resized, imagecolorat($resized, $x, $y));
				$row[$x] = floor(($rgb['red'] * 0.299) + ($rgb['green'] * 0.587) + ($rgb['blue'] * 0.114));
			}
			$rows[$y] = $this->DCT1D($row);
		}

		// Free up memory.
		imagedestroy($resized);

		// Calculate the DCT for each column.
		for ($x = 0; $x < static::SIZE; $x++) {
			for ($y = 0; $y < static::SIZE; $y++) {
				$col[$y] = $rows[$y][$x];
			}
			$matrix[$x] = $this->DCT1D($col);
		}

		// Extract the top 8x8 pixels.
		$pixels = [];
		for ($y = 0; $y < 8; $y++) {
			for ($x = 0; $x < 8; $x++) {
				$pixels[] = $matrix[$y][$x];
			}
		}

		// Calculate the median.
		$median = $this->median($pixels);

		// Calculate hash.
		$hash = 0;
		$one = 1;
		foreach ($pixels as $pixel) {
			if ($pixel > $median) {
				$hash |= $one;
			}
			$one = $one << 1;
		}

		return $hash;
	}

	/**
	 * Perform a 1 dimension Discrete Cosine Transformation.
	 *
	 * @param array $pixels
	 */
	protected function DCT1D(array $pixels){
		$transformed = [];
		$size = count($pixels);

		for ($i = 0; $i < $size; $i++) {
			$sum = 0;
			for ($j = 0; $j < $size; $j++) {
				$sum += $pixels[$j] * cos($i * pi() * ($j + 0.5) / ($size));
			}

			$sum *= sqrt(2 / $size);

			if ($i == 0) {
				$sum *= 1 / sqrt(2);
			}

			$transformed[$i] = $sum;
		}

		return $transformed;
	}

	/**
	 * Get the median of the pixel values.
	 *
	 * @param  array $pixels
	 * @return float
	 */
	protected function median(array $pixels){
		sort($pixels, SORT_NUMERIC);
		$middle = floor(count($pixels) / 2);

		if (count($pixels) % 2) {
			$median = $pixels[$middle];
		} else {
			$low = $pixels[$middle];
			$high = $pixels[$middle + 1];
			$median = ($low + $high) / 2;
		}

		return $median;
	}
}
