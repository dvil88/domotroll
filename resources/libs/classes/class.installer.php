<?php
class installer{
	private $conn = false;
	private $rootPassword;

	public $distribution;
	public $webServer;
	private $installer;

	public function __construct(){
	}

	public function sshConnect($host, $user, $pass){
		$this->rootPassword = $pass;

		$this->ssh2 = new _ssh2($host, ['user'=>$user, 'pass'=>$pass]);
		$connection = $this->ssh2->connect();
		if( isset($connection['error']) ){ return $connection; }

		return true;
	}

	public function sshDisconnect(){
		$this->ssh2->disconnect();
	}

	public function getDistribution(){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		$this->systemNameId = $this->ssh2->command('cat /etc/os-release | grep -oP "^ID=\"\K[^\"]+|^ID=\K.*"');
		$this->version = $this->ssh2->command('cat /etc/os-release | grep -oP "^VERSION_ID=\"\K[^\"]+|^VERSION_ID=\K.*"');
		$this->distribution = $this->ssh2->command('hostnamectl | grep -oP  --color=never "Operating System:\s*\K.*"');

		switch( $this->systemNameId ){
			case 'centos':
				include_once('classes/installer/class.centos.php');
				$this->installer = new centos($this->ssh2, $this->rootPassword);
				break;
			case 'debian':
				include_once('classes/installer/class.debian.php');
				$this->installer = new debian($this->ssh2, $this->rootPassword);
				break;
			case 'ubuntu':
				include_once('classes/installer/class.ubuntu.php');
				$this->installer = new ubuntu($this->ssh2, $this->rootPassword);
				break;
		}
	}

	public function getWebServer(){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		$this->webServer = $this->installer->getWebServer();
	}

	public function getWebServerUser(){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		$this->webServerUser = $this->installer->command('lsof -i -P -n | grep --color=never "\*:80 " | awk \'{print $3}\' | grep -v "root" | awk -F, \'!seen[$1]++\'');
	}

	public function checkPackages(){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }


		$this->installer->installPHPRepository();
		$this->installer->checkPackages();
	}

	public function installPackages(){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		$this->installer->installPackages();
		$this->getWebServer();
		$this->getWebServerUser();
	}

	public function configureVhost($host, $path){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		if( $this->webServer == 'nginx' ){
			$this->installer->configureVhostNginx($host, $path);
		} else if( $this->webServer == 'apache2' ){
			$this->installer->configureVhostApache($host, $path);
		} else if( $this->webServer == 'httpd' ){
			$this->installer->configureVhostHttpd($host, $path);
		}

	}

	public function checkAsimovInstall($path){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }

		$asimovInstalled = $this->ssh2->directory_exists($path);
		if( $asimovInstalled ){
			$configFileExists = $this->ssh2->file_exists($path.'/resources/db/config.php');
			if( $configFileExists ){
				return ['error'=>true, 'errorDescription'=>'Asimov already installed'];
			}
		}

		return ['error'=>false];
	}

	public function installAsimov($path){
		$this->installer->command('git clone https://dvil88:55ChhKMbuLjNCBaF2x_k@gitlab.com/dvil88/asimov.git '.$path);
	}

	public function createConfigFile($config){
		if( !$this->ssh2 ){ return ['error'=>true, 'errorDescription'=>'Not connected']; }



		// ['{{dbConfig_host}}','{{dbConfig_user}}','{{dbConfig_pass}}','{{dbConfig_database}}']
		// ['{{mongoConfig_host}}','{{mongoConfig_port}}','{{mongoConfig_user}}','{{mongoConfig_pass}}','{{mongoConfig_database}}']
		// ['{{redisConfig_host}}','{{redisConfig_port}}','{{redisConfig_pass}}']
		// ['{{ldapConfig_host}}','{{ldapConfig_rdn}}','{{ldapConfig_dn}}','{{ldapConfig_attributes}}']
	}

	public function transferConfigFile($path){
		$this->ssh2->file_send('../db/config.php', $path.'/resources/db/config.php');

		// $this->ssh2->file_send('../db/config.php', '/tmp/config.php');
		// $this->installer->command('mv /tmp/config.php '.$path.'/resources/db/config.php');
	}

	public function saveServerId($path, $serverId){
		$this->ssh2->file_put($path.'/resources/db/.serverId', $serverId);
	}

	public function setPermissions($user, $path){
		$this->installer->command('chown -R '.$user.':'.$user.' '.$path);
	}

	public function installCrontab($path){
		$this->installer->command('crontab -l | { cat; echo "* * * * * nohup php '.$path.'/resources/cli/cli.proc.php daemon &"; } | crontab -');
	}

	public function installGlobalProcesses($serverId){
		$_proc = new _proc();
		$globalProcesses = $_proc->getWhere(['serverId'=>'global']);

		foreach( $globalProcesses as $process ){
			unset($process['_id']);
			$process['serverId'] = $serverId;
			$r = $_proc->save($process);
		}
	}
}