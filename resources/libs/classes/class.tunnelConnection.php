<?php

class tunnelConnection {
	private $container;

	private $tunnelsAllowed;
	private $tunnels = [];

	private $openvpnCommand = 'nohup /usr/sbin/openvpn --config /etc/openvpn/scripts/winds.ovpn --remote {host} {port} --dev {tunnel} --auth-user-pass /etc/openvpn/scripts/login_winds.conf';

	public function __construct() {
		$this->vpnTunnelsTB = new vpnTunnels_TB();
		$this->processTunnelsTB = new processTunnels_TB();
	}

	public function getTunnelsAllowed(){
		$tunnels = $this->vpnTunnelsTB->getWhere(['status'=>'up'], ['sort'=>'tunnelId ASC']);

		$this->tunnelsAllowed = [];
		foreach( $tunnels as $tunnel ){
			$this->tunnelsAllowed[] = 'tun'.$tunnel['tunnelId'];
		}

		return $this->tunnelsAllowed;
	}

	public function openTunnel($tunnel) {
		if( !$this->isTunnelUp($tunnel) ){
			$tunnelId = (int)str_replace('tun', '', $tunnel);

			$tunnelOut = '/dev/shm/tun'.$tunnelId.'.out';
			if( file_exists($tunnelOut) ){ unlink($tunnelOut); }

			$tunnelOB = $this->vpnTunnelsTB->getSingle(['tunnelId'=>$tunnelId]);
			
			$command = str_replace(
				['{host}', '{port}', '{tunnel}'],
				[$tunnelOB['url'], $tunnelOB['port'], 'tun'.$tunnelOB['tunnelId']],
				$this->openvpnCommand
			);

			$cmd = $command.' 2>/dev/null > '.$tunnelOut.' &';
			exec($cmd);
			$tries = 0;
			do{
				$execOut = file_get_contents($tunnelOut);
				if( strpos($execOut, 'Tunnel established') !== false ){
					unlink($tunnelOut);

					$ip = (new curl(['interface'=>'tun'.$tunnelId]))->get('http://serieslab.com/ip.php')['pageContent'];

					$this->vpnTunnelsTB->findAndModify(['_id'=>$tunnelOB['_id']], ['$set'=>['status'=>'up', 'ip'=>$ip]], ['upsert'=>false]);
					return true;
				}

				sleep(1);
			} while(true && $tries++ < 30 );
			unlink($tunnelOut);
			$this->vpnTunnelsTB->findAndModify(['_id'=>$tunnelOB['_id']], ['$set'=>['status'=>'down']], ['upsert'=>false]);
			return false;
		}
	}

	public function registerProcessTunnel($pid, $tunnel){
		$processTunnelOB = $this->processTunnelsTB->getSingle(['pid'=>$pid]);
			$processTunnelOB = array_merge($processTunnelOB, [
			'pid'=>$pid,
			'tunnel'=>$tunnel,
		]);

		$this->processTunnelsTB->save($processTunnelOB);
	}

	public function getTunnelsByProcess($process){
		$command = 'ps aux | grep -v grep | grep '.$process.' | awk \'{print $2}\'';
		$procs = shell_exec($command);

		$procs = array_diff(explode("\n", $procs), ['']);

		$orQuery = [];
		foreach( $procs as $pid ){
			$orQuery[] = ['pid'=>$pid];
		}
		$processes = $this->processTunnelsTB->getWhere(['$or'=>$orQuery]);

		$tunnels = [];
		foreach( $processes as $proc ){
			$tunnels[$proc['pid']] = 'tun'.$proc['tunnel'];
		}

		return $tunnels;
	}

	public function getRandomTunnelForProcess($process, $bannedTunnels = []){
		$this->getTunnelsAllowed();

		$tunnels = $this->getTunnelsByProcess($process);

		$availableTunnels = array_diff($this->tunnelsAllowed, $tunnels);
		if( $bannedTunnels ){
			$availableTunnels = array_diff($availableTunnels, $bannedTunnels);
		}
		
		if( !$availableTunnels ){ return false; }

		$tunnel = $availableTunnels[array_rand($availableTunnels)];
		return $tunnel;
	}

	protected function closeDown($tunnel) {
		$command = "COLUMNS=9999 ps ax | grep -v grep | grep openvpn | grep ' $tunnel ' | awk '{print $1}' | xargs -r kill";
		shell_exec($command);
	}

	public function setRestart($tunnel){
		$tunnel = (int)str_replace('tun', '', $tunnel);
		$tunnelOB = $this->vpnTunnelsTB->getSingle(['tunnelId'=>$tunnel]);
		$this->vpnTunnelsTB->findAndModify(['_id'=>$tunnelOB['_id']], ['$set'=>['status'=>'restart']]);
	}

	public function launchRestart(){
		$tunnels = $this->vpnTunnelsTB->getWhere(['status'=>'restart'], ['sort'=>'tunnelId ASC']);
		foreach( $tunnels as $tunnel ){
			$this->restartTunnel('tun'.$tunnel['tunnelId']);
		}
	}

	protected function restartTunnel($tunnel) {
		$this->closeDown($tunnel);
		sleep(3);
		// sleep(30);
		$this->openTunnel($tunnel);
	}

	public function isTunnelUp($tunnel) {
		if( !file_exists('/sys/class/net/'.$tunnel) ){
			return false;
		}
		return true;
		/*
        // So, we have to add additional 'grep -v grep' filter that will remove process which command line contains 'grep' i.e. grep itself.
        $command = "ps aux | grep -v grep | grep openvpn | grep $tunnel | wc -l";
        return exec($command);
        */
	}

	public function getOpenTunnels() {
		$command = "COLUMNS=9999 ps ax | grep -v grep | grep openvpn | awk '{print $12}'";
		$ret = trim(shell_exec($command));
		$tunnels = explode("\n", $ret);

		return $tunnels;
	}

	public function closeAll() {
		$execCMD = 'killall openvpn > /dev/null &';

		exec($execCMD);
	}
}
