<?php

class curl{
	public $options = [
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_FOLLOWLOCATION => 1,
		CURLOPT_USERAGENT  => 'Mozilla/5.0 (X11; Linux x86_64; rv:56.0) Gecko/20100101 Firefox/56.0',
		CURLOPT_HEADER   => 1,
		CURLOPT_ENCODING  => 'gzip,deflate',
		CURLOPT_SSL_VERIFYHOST => 2,
		CURLOPT_SSL_VERIFYPEER => true,
		// CURLOPT_SSL_CIPHER_LIST => 'TLSv1',
		CURLOPT_CONNECTTIMEOUT => 30,
		CURLOPT_TIMEOUT   => 60,
		CURLINFO_HEADER_OUT  => true,
		CURLOPT_VERBOSE   => false,
	];

	/**
	 * __construct
	 *
	 * @param unknown $config (optional)
	 */
	public function __construct($config = []) {
		if (isset($config['cookieFile'])) {
			$this->options[CURLOPT_COOKIEFILE] = $config['cookieFile'];
			$this->options[CURLOPT_COOKIEJAR] = $config['cookieFile'];
		}

		if (isset($config['proxy'])) {
			$proxy = $config['proxy'];
			$this->options[CURLOPT_PROXY] = $proxy['host'].':'.$proxy['port'];
			$this->options[CURLOPT_PROXYTYPE] = $proxy['type'];
			$this->options[CURLOPT_HTTPPROXYTUNNEL] = 1;
		}

		if (isset($config['cert'])) {
			$this->options[CURLOPT_CAINFO] = $config['cert']['ca'];
			$this->options[CURLOPT_SSLKEY] = $config['cert']['key'];
			$this->options[CURLOPT_SSLCERT] = $config['cert']['cert'];
			$this->options[CURLOPT_SSL_VERIFYHOST] = false;
			$this->options[CURLOPT_SSL_VERIFYPEER] = false;
			unset($this->options[CURLOPT_SSL_CIPHER_LIST]);
		}

		if (isset($config['interface'])) {
			$this->options[CURLOPT_INTERFACE] = $config['interface'];
		}
	}

	public function setInterface($interface){
		$this->options[CURLOPT_INTERFACE] = $interface;
	}


	/**
	 * query
	 *
	 * @param unknown $url
	 * @param unknown $data (optional)
	 * @return unknown
	 */
	public function query($url, $data = []) {
		return $this->get($url, $data);
	}


	/**
	 * get
	 *
	 * @param unknown $url
	 * @param unknown $data (optional)
	 * @return unknown
	 */
	public function get($url, $data = []) {
		$url = trim($url);
		// $data['currentUrl'] = $data['referer'] = $url;
		$data['currentUrl'] = $url;

		// Config curl options
		$opts = $this->configCurl($data);
		// print_r($opts);

		// $this->options[CURLOPT_URL] = $url;
		$opts[CURLOPT_URL] = $url;
		$uinfo = parse_url($url);



		// Make petition
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		// echo $url.(isset($data['interface']) ? ' through '.$data['interface'] : '').PHP_EOL;
		// echo 'Memory before Curl: '.round((memory_get_usage() / 1048576),2).'Mb'.PHP_EOL;
		$res = curl_exec($ch);
		// echo 'Memory after Curl: '.round((memory_get_usage() / 1048576),2).'Mb'.PHP_EOL.PHP_EOL;
		$info = curl_getinfo($ch);

		if ( !$res ) {
			$errorCode = curl_errno($ch);
			$errorDescription = curl_error($ch);
			return ['pageCode'=>$errorCode, 'errorCode'=>$errorCode, 'errorDescription'=>$errorDescription];
		}

		list($pageHeader, $pageContent) = explode("\r\n\r\n", $res, 2);
		if (stripos($pageHeader, '100 continue') !== false) {list($pageHeader, $pageContent) = explode("\r\n\r\n", $pageContent, 2);}

		if (isset($data['followLocation']) && $data['followLocation'] && preg_match('/location: (.*)/i', $pageHeader, $follow)) {
			if (isset($data['post'])) {unset($data['post']);}
			if (strpos($follow[1], 'http') !== false) {return $this->query($follow[1], $data);}
			return $this->query($uinfo['scheme'].'://'.$uinfo['host'].'/'.$follow[1], $data);
		}

		// $return = array_merge($data,['pageCode'=>$info['http_code'],'pageHeader'=>$pageHeader,'pageContent'=>$pageContent]);
		$return = array_merge($data, ['pageCode'=>$info['http_code'], 'requestHeader'=>(isset($info['request_header']) ? $info['request_header'] : ''), 'pageHeader'=>$pageHeader, 'pageContent'=>$pageContent]);
		return $return;
	}

	public function post($url, $postData, $data = []){
		$data = array_merge($data, ['post'=>$postData]);
		return $this->get($url, $data);
	}


	/**
	 * configCurl
	 *
	 * @param unknown $data
	 * @return unknown
	 */
	private function configCurl($data) {
		$opts = $this->options;

		if (isset($data['proxy'])) {
			$proxy = $data['proxy'];
			$opts[CURLOPT_PROXY] = $proxy['host'].':'.$proxy['port'];
			$opts[CURLOPT_PROXYTYPE] = $proxy['type'];
			$opts[CURLOPT_HTTPPROXYTUNNEL] = 1;
		}

		if (isset($data['interface'])) {
			$opts[CURLOPT_INTERFACE] = $data['interface'];
		}

		if (isset($data['header'])) {
			$opts[CURLOPT_HTTPHEADER] = $data['header'];
		}

		if (isset($data['post'])) {
			$postString = $data['post'];
			if (is_array($data['post'])) {$postString = http_build_query($data['post']);}
			$opts[CURLOPT_POST] = true;
			$opts[CURLOPT_POSTFIELDS] = $postString;
		}

		if (isset($data['put'])) {
			$opts[CURLOPT_POSTFIELDS] =  $data['put'];
			$opts[CURLOPT_CUSTOMREQUEST] = 'PUT';
		}

		if (isset($data['files'])) {
			$localFile = $data['files']['file'];
			$fp = fopen($localFile, 'r');

			$opts[CURLOPT_INFILE] = $fp;
			$opts[CURLOPT_INFILESIZE] = filesize($localFile);
		}

		if (isset($data['referer'])) {
			$opts[CURLOPT_REFERER] = $data['referer'];
		}
		/*
		if(isset($data['cookies']) && count($data['cookies']) > 0){
			$cookieData = '';foreach($data['cookies'] as $cookie){list($key,$value) = each($cookie);$cookieData .= $key.'='.$value.'; ';}
			if($cookieData == ''){$cookieData = substr($cookieData,0,-2);}
			$this->options[CURLOPT_COOKIE] = $cookieData;
		}
		*/

		return $opts;
	}
}