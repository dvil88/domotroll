<?php

class vpnTunnels_TB extends _mongo{
	public $table = 'vpnTunnels';
	public $fields = [
		'_id'=>'TEXT',
		'tunnelId'=>'TEXT',
		'country'=>'TEXT',
		'city'=>'TEXT',
		'url'=>'TEXT',
		'port'=>'TEXT',
		'status'=>'TEXT',
		'ip'=>'TEXT',
	];
	public $indexes = [
		['fields'=>['tunnelId'=>1],'props'=>['unique'=>true]],
		['fields'=>['url'=>1],'props'=>['unique'=>true]],
		['fields'=>['status'=>1],'props'=>[]],
		['fields'=>['ip'=>1],'props'=>[]],
	];

	public function __construct(){
		$this->server = 'mongodb://'.config::$mongoConfig['user'].':'.config::$mongoConfig['pass'].'@'.config::$mongoConfig['host'].':'.config::$mongoConfig['port'];
		$this->db = config::$mongoConfig['database'];
	}

	function validate(&$data = [],&$oldData = []){
		return $data;
	}
}