<?php

class devices_TB extends _mongo{
	public $table = 'devices';
	public $fields = [
		'_id'=>'TEXT',

		'name'=>'TEXT',
		'ip'=>'TEXT',
		'type'=>'TEXT',

		'date'=>'TEXT',
	];
	public $indexes = [
	];

	public function __construct(){
		$this->server = 'mongodb://'.config::$mongoConfig['user'].':'.config::$mongoConfig['pass'].'@'.config::$mongoConfig['host'].':'.config::$mongoConfig['port'];
		$this->db = config::$mongoConfig['database'];
	}

	function validate(&$data = [],&$oldData = []){

		if( !isset($data['date']) ){ $data['date'] = time(); }
		return $data;
	}
}