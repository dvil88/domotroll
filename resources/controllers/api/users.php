<?php
include_once('api.php');

class users extends _api{
	public $_preffix = '/api/users/';

	public function list(){
		if( empty($this->_users_TB) ){ $this->_users_TB = new users_TB(); }

		if( !$this->_users_TB->isLogged() || !$this->_users_TB->isAdmin() ){ return $this->_error('E_UNAUTHORIZED'); }
		
		$itemsPerPage = config::$pageSize;
		$limit  = (($this->_page - 1) * $itemsPerPage).','.$itemsPerPage;
		
		$clause = [];
		$params = ['limit'=>$limit, 'sort'=>['_id'=>1]];

		/*if (!empty($_GET['sort'])
			 && is_string($_GET['sort'])
			 && preg_match('!^(?<field>_id|profileUsed),(?<order>ASC|DESC)$!',$_GET['sort'],$m)) {
				$order = str_replace(['DESC','ASC'],['-1','1'],$m['order']);
				$params['sort'] = [$m['field']=>intval($order)];
			}*/

		
		$pagerTotal = $this->_users_TB->count($clause);

		$userOBs = $this->_users_TB->getWhere($clause, $params);
		$result = array_map([$this, '_format_user'], $userOBs);

		return $this->_result($result, [
			'page'=>$this->_page,
			'total'=>$pagerTotal, 
			'items'=>$itemsPerPage
		]);
	}

	public function login(){
		if( !$this->_input ){ return $this->_error('E_INVALID'); }

		if( empty($this->_users_TB) ){ $this->_users_TB = new users_TB(); }
		$userOB = $this->_users_TB->login($this->_input['userName'], $this->_input['userPass']);
		if( isset($userOB['errorDescription']) ){
			if( $userOB['errorDescription'] == 'USER_ERROR' || $userOB['errorDescription'] == 'PASSWORD_ERROR' ){
				return $this->_error('E_USER_PASSWORD');
			}
		}
		
		$result = $this->_format_user($userOB);
		return $this->_result($result);
	}

	public function me(){
		if( empty($this->_users_TB) ){ $this->_users_TB = new users_TB(); }

		if( !$this->_users_TB->isLogged() ){ return $this->_error('E_UNAUTHORIZED'); }
		$userOB = $GLOBALS['user'];

		$result = $this->_format_user($userOB);
		return $this->_result($result);
	}

	public function user($id = '', $method = ''){
		if( empty($this->_users_TB) ){ $this->_users_TB = new users_TB(); }

		if( $method != 'POST' && !isset($this->_input['retrieve']) && !($this->_userOB = $this->_users_TB->getByID($id)) ){
			$this->_error('E_NOT_FOUND');
		}

		if( !empty($method) ){
			switch( $method ){
				case 'GET':
					$result = $this->_format_user($this->_userOB);
					$this->_result($result);
					break;
				case 'POST':
				case 'PUT':
					if ($method == 'POST') {
						/* Si vamos a insertar un nuevo elemento no
						 * puede existir el '_id' de ninguna de las maneras */
						unset($this->_input['_id']);
					}

					/* Re-prefix */
					$_valid = [
						 '_id'
						,'userName'
						,'userMail'
						,'userStatus'
						,'userPass'
						,'userSalt'
						,'userModes'
						,'name'
						,'surname'
					];
					foreach ($this->_input as $k=>$v) {if (!in_array($k,$_valid)) {unset($this->_input[$k]);}}
					
					if( !empty($this->_input['userPass']) ){ $this->_input['userSalt'] = false; }
					else {unset($this->_input['userPass']);}

					$r = $this->_users_TB->save($this->_input);
					if (isset($r['errorDescription'])) {
						$this->_error($r['errorDescription']);
					}

					/* Esta linea es para devolver correctamente cuando
					 * se inserte uno nuevo (método POST) */
					$id = strval($this->_input['_id']);
					return call_user_func_array([$this,__FUNCTION__],[$id,'GET']);
					break;
				case 'DELETE':
					$r = $this->_users_TB->removeByID($id);
					if (isset($r['errorDescription'])) {
						$this->_error($r['errorDescription']);
					}
					$this->_result([]);
					
					break;
			}
		}
	}

	/*protected function _format_user_login($userOB){
		return array_filter([
			'_id'=>				strval($userOB['_id']),
			'name'=>			$userOB['name'],
			'surname'=>			$userOB['surname'],
			'fullName'=>		$userOB['name'].' '.$userOB['surname'],
			// 'telegramId'=>		$userOB['telegramId'],
			// 'userDate'=>		$userOB['userDate'],
			// 'userLastLogin'=>	$userOB['userLastLogin'],
			'userMail'=>		$userOB['userMail'],
			'userModes'=>		$userOB['userModes'],
			'userName'=>		$userOB['userName'],
		], function($n){ return !is_null($n); });
	}*/

	protected function _format_user($userOB){
		return array_filter([
			'_id'=>				strval($userOB['_id']),
			'name'=>			isset($userOB['name']) ? $userOB['name'] : null,
			'surname'=>			isset($userOB['surname']) ? $userOB['surname'] : null,
			'fullName'=>		isset($userOB['name']) ? $userOB['name'].' '.$userOB['surname'] : null,
			'userName'=>		isset($userOB['userName']) ? $userOB['userName'] : null,
			'userMail'=>		isset($userOB['userMail']) ? $userOB['userMail'] : null,
			'userModes'=>		isset($userOB['userModes']) ? $userOB['userModes'] : null,
			'userStatus'=>		isset($userOB['userStatus']) ? $userOB['userStatus'] : null,
			'telegramId'=>		isset($userOB['telegramId']) ? $userOB['telegramId'] : null,
			'telegramToken'=>	isset($userOB['telegramToken']) ? $userOB['telegramToken'] : null,
			'userDate'=>		isset($userOB['userDate']['stamp']) ? date('d/m/Y H:i:s', $userOB['userDate']['stamp']) : null,
			'userLastLogin'=>	isset($userOB['userLastLogin']) ? date('d/m/Y H:i:s', $userOB['userLastLogin']) : null,
		], function($n){ return !is_null($n); });
	}
}