<?php

class index extends common{
	private $usersTB;

	public function __construct(){
		parent::__construct();

		$this->usersTB = new users_TB();

		/* INI-Check userLogin */
		// if( !$this->usersTB->isLogged() ){
		// 	return $this->r($this->template['w.indexURL'].'/user/login');
		// }
		// $this->template['userName'] = $GLOBALS['user']['name'];
		// $this->template['userInitials'] = $GLOBALS['user']['initials'];
		// $this->template['w.admin'] = $this->usersTB->isAdmin();
		/* END-Check userLogin */
		
		$this->template['PAGE.CLASS'] = 'index';
		$this->template['PAGE.TITLE'] = 'DomoTroll';
	}

	public function main($param = false){
		return $this->renderTemplate('index');
	}

	public function view($id = false){
		
	}
}