<?php
	if( isset($GLOBALS['w.localhost']) && $GLOBALS['w.localhost'] ){ ini_set('display_errors',1); }
	date_default_timezone_set('Europe/Madrid');

	if( file_exists('../db/config.php') ){
		include_once('../db/config.php');
	}
	// include_once('inc.common.php');
	include_once('class.common.php');

	if( !function_exists('mb_strlen') ){echo 'Please install php-mbstring'.PHP_EOL;exit;}

	spl_autoload_register(function( $name ){
		switch( $name ){
			/* INI-Databases */
			case '_mongo':
			case '_mongodb':
				if( class_exists('MongoId') ){include('classes/class._mongo.php');break;}
				include('classes/class._mongodb.php');break;
			case '_sqlite3':
				if( !class_exists('SQLite3') ){echo 'Please install php-sqlite3'.PHP_EOL;exit;}
				include('classes/class._sqlite3.php');break;
			case '_mysql':				include('classes/class._mysql.php');break;
			/* END-Databases */

			case '_api':					include('classes/class._api.php');break;
			case '_html_fileg':
			case '__html_fileg':			include('classes/class._html_fileg.php');break;
			case '_html_tfrog':
			case '__html_tfrog':			include('classes/class._html_tfrog.php');break;
			case '_html_curl':
			case '__html_curl':				include('classes/class._html_curl.php');break;
			// case '__strings':				include('inc.strings.php');break;
			case '__strings':				include('classes/class._strings.php');break;
			case '_date':
			case '__date':					include('classes/class._date.php');break;
			case '_params':					include('classes/class._params.php');break;
			case '__images':				include('classes/class._images.php');break;
			case '_path':					include('classes/class._path.php');break;
			case '_task':
			case '_worker':
			case '_proc':
			case '_proc_utils':				include('classes/class._proc.mongo.php');break;
			case '_ssh2':					include('classes/class._ssh2.php');break;
			case '_zip':
				if( !class_exists('ZipArchive') ){echo 'Please install php-zip'.PHP_EOL;exit;}
				include('classes/class._zip.php');break;
				
			case '_google':					include('classes/class._google.php');break;
			case '_google_gmail':			include('classes/google/class._google_gmail.php');break;

			case '_shoutbox_sqlite3':		include('classes/class._shoutbox.sqlite3.php');break;
			case 'XLSXWriter':				include('classes/class.XLSXWriter.php'); break;

			case 'curl':					include('classes/class.curl.php'); break;
			case 'logger':					include('classes/class.logger.php'); break;
			case 'tunnelConnection':		include('classes/class.tunnelConnection.php'); break;
			
			
			case 'imageHash':				include('classes/class.imageHash.php');break;
			case 'valuation':				include('classes/class.valuation.php');break;

			case 'users_TB':				include('api.users.mongo.php'); break;
			case 'processTunnels_TB':		include('api.processTunnels.php'); break;

			case 'alerts_TB':				include('api.alerts.php'); break;
			case 'devices_TB':				include('api.devices.php'); break;
			
			case 'statsPages_TB':
			case 'statsHashes_TB':			include('api.stats.php'); break;
		}
	});
