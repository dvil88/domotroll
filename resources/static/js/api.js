	var _api = {};
	_api.__graph = '';
	_api.base = '/api/';
	_api.query = function(url,options){
		if (!options) {options = {};}
		if (!options.headers) {options.headers = {};}

		if (_api.__graph.length) {
			options.headers.graph = _api.__graph;
			_api.__graph = '';
		}

		// options.headers.profile = _api.__profile;
		options.credentials = 'include';

		return new Promise((resolve,reject) => {
			fetch(url,options)
			.then((res) => {return res.json();})
			.then((res) => {return resolve(res);})
			.catch(res => reject(res));
		});
	};
	_api.query_save = function(_url,_save){
		const _options = {
			 method: (_save._id && _save._id.length) ? 'PUT' : 'POST'
			,body: JSON.stringify(_save)
			,headers: {'Content-Type': 'application/json'}
		};

		return _api.query(_url,_options);
	};
	_api.query_delete = function(_url){
		const _options = {
			 method: 'DELETE'
			,headers: {'Content-Type': 'application/json'}
		};

		return _api.query(_url,_options);
	};
	_api._graph = function(_graph){
		_api.__graph = _graph;
		return _api;
	};



	_api.cron = function(){
		var _url = _api.base + 'cron/';
		return {
			'getProcesses': (_params = new URLSearchParams()) => {return _api.query(_url + 'getProcesses' + '?' + _params.toString());},
			'getWorkers': (_params = new URLSearchParams()) => {return _api.query(_url + 'getWorkers' + '?' + _params.toString());},
			'getServers': (_params = new URLSearchParams()) => {return _api.query(_url + 'getServers' + '?' + _params.toString());},
			'getDaemon': (_params = new URLSearchParams()) => {return _api.query(_url + 'getDaemon' + '?' + _params.toString());},

			'scheduleLaunch': (_data) => { return _api.query_save(_url + 'scheduleLaunch', _data); },
			'scheduleLaunchFix': (_data) => { return _api.query_save(_url + 'scheduleLaunchFix', _data); },
			'scheduleRemove': (_data) => { return _api.query_save(_url + 'scheduleRemove', _data); },
			'scheduleDisable': (_data) => { return _api.query_save(_url + 'scheduleDisable', _data); },
			'scheduleEnable': (_data) => { return _api.query_save(_url + 'scheduleEnable', _data); },
			'cronLaunch': (_data) => { return _api.query_save(_url + 'cronLaunch', _data); },
			'workerSave': (_data) => { return _api.query_save(_url + 'workerSave', _data); },
			'workerKill': (_data) => { return _api.query_save(_url + 'workerKill', _data); },
			'workerClean': (_data) => { return _api.query_save(_url + 'workerClean', _data); },
		}
	};




	_api.cars = function(){
		var _url = _api.base + 'cars';

		return {
			'list': (_params = new URLSearchParams()) => {return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' ));},
			'match': (_params = new URLSearchParams()) => {return _api.query(_url + '/match' + ( _params ? '?'+_params.toString() : '' ));},
			'edit': (_params = new URLSearchParams()) => {return _api.query(_url + '/edit' + ( _params ? '?'+_params.toString() : '' ));},
			'save': (_data) => { return _api.query_save(_url + '/save', _data); },
			// 'create': (_data) => {return _api.query_save(_url + 'create/', _data);},
		}
	};


	_api.lstns = function(){
		var _url = _api.base + 'lstns';

		return {
			'filters': 	() => { return _api.query(_url + '/filters'); },
			'list': 	(_params) => { return _api.query(_url + '/list' + ( _params ? '?' + _params.toString() : '' )); },
			'total': 	(_params) => { return _api.query(_url + '/total' + ( _params ? '?' + _params.toString() : '' )); },
			'brands': 	() => { return _api.query(_url + '/brands'); },
			'models': 	(_params) => { return _api.query(_url + '/models' + ( _params ? '?' + _params.toString() : '' )); },
			'alert': 	(_params) => { return _api.query(_url + '/alert' + ( _params ? '?' + _params.toString() : '' )); },
			// 'list': 	(_listingId) => { return _api.query(_url + '/list/' + _listingId); },
		}
	};

	_api.lstn = function(_listing){
		if (!_listing) {_listing = '';}
		var _url = _api.base + 'lstn';

		return {
			'get': () => {return _api.query(_url + '/list/' + _listing);},
			'raw': () => {return _api.query(_url + '/raw/' + _listing);},
		};
	};

	_api.brnds = function(){
		var _url = _api.base + 'brnds';

		return {
			'list': (_params) => { return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' )); },
			'filter': (_params) => { return _api.query(_url + '/filter'); },
			'brand': (_brand = '') => {
				_url += '/brand/' + _brand;
				return {
					'get': () => { return _api.query(_url); },
					'models': (_params) => { return _api.query(_url + '/models' + ( _params ? '?'+_params.toString() : '' )); },
					'edit': () => { return _api.query(_url + '/edit'); },
					'match': () => { return _api.query(_url + '/match'); },
					'save': (_save) => { return _api.query_save(_url,_save); },
				}
			},
		};
	};

	_api.mdls = function(){
		var _url = _api.base + 'mdls';
		return {
			'list': (_params) => { return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' )); },
			'filter': (_params) => { return _api.query(_url + '/filter' + ( _params ? '?'+_params.toString() : '' )); },
		};
	};

	_api.pool = function(){
		var _url = _api.base + 'pool';

		return {
			'brands': (_params = new URLSearchParams()) => {return _api.query(_url + '/brands' + ( _params ? '?'+_params.toString() : '' ));},
			'models': (_params = new URLSearchParams()) => {return _api.query(_url + '/models' + ( _params ? '?'+_params.toString() : '' ));},
			'engines': (_params = new URLSearchParams()) => {return _api.query(_url + '/engines' + ( _params ? '?'+_params.toString() : '' ));},
			// 'create': (_data) => {return _api.query_save(_url + 'create/', _data);},
		};
	};

	_api.sllrs = function(){
		var _url = _api.base + 'sllrs';

		return {
			'list': (_params) => { return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' ));},
		};
	};

	_api.users = function(){
		var _url = _api.base + 'users';

		return {
			'login': (_data) => { return _api.query_save(_url + '/login', _data); },
			'me': () => { return _api.query(_url + '/me'); },
			'list': (_params = new URLSearchParams()) => {return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' ));},
			'create': (_data) => { return _api.query_save(_url + '/user', _data); },
			// 'user': function(_user){
			// 	_url += '/user/' + _user;
			// 	return {
			// 	};
			// },
		}
	};

	_api.prfl = function(_profile){
		if (!_profile) {_profile = '';}
		var _url = _api.base + 'prfl/' + _profile;

		return {
			'list': (_params = new URLSearchParams()) => {return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' ));},
			'create': (_data) => { return _api.query_save(_url + '/profile', _data); },
			'user': function(_user){
				_url += '/user/' + _user;
				return {
					'save': function(){
						var _method = 'POST';

						const _options = {
							method: _method,
							headers: {'Content-Type': 'application/json'}
						};
						return _api.query(_url,_options);
					},
					'remove': function(){return _api.query_delete(_url);}
				};
			}
		}
	};

	_api.alrts = function(){
		var _url = _api.base + 'alrts';

		return {
			'create': (_data) => { return _api.query_save(_url + '/alert', _data); },
		}

	}


	_api.stats = function(){
		var _url = _api.base + 'stats';

		return {
			'list': (_params = new URLSearchParams()) => {return _api.query(_url + '/list' + ( _params ? '?'+_params.toString() : '' ));},
		}
	}